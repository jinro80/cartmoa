package com.chris.cartmoa.CartScript;

/**
 * Created by phj on 2017-01-24.
 */

public class ScriptUtil
{

    public static IWebSrcipt ScriptFactory(String market)
    {
        switch (market)
        {
            case "G마켓" :

                return new GmarketScript();

            case "11번가" :

                return new ElevenStreetScript();

            case  "옥션" :
                return  new AuctionScript();

            case  "인터파크" :
                return  new InterParkScript();

            case  "롯데홈쇼핑":
                return new LotteIMallScript();

            case "롯데마트":
                return  new LotteMartScript();

            case "롯데닷컴":
                return  new LotteComScript();

            case "현대H몰":
                return  new HmallScript();

            case "GSSHOP":
                return  new GSShopScript();

            case "이마트몰":
                return  new EmartScript();

            case "홈플러스":
                return  new HomeplusScript();

            case "SSG":
                return  new SSGScript();

            case "더현대닷컴":
                return  new TheHyundaiScript();

            case "올리브영":
                return  new OliveyoungScript();

            case "CJ몰":
                return  new CJMallScript();

            case "AK몰":
                return  new AKMallScript();

            case "쿠팡":
                return  new CoupangScript();

            case "G9":
                return new G9Script();

            case "위메프":
                return new WeMakePriceScript();

            case "티몬":
                return  new TmonScript();

            case "NS홈쇼핑":
                return  new NSHomeShoppingScript();

            case "홈앤쇼핑":
                return  new HomeAndShoppingScript();

            case "K쇼핑":
                return  new KShoppingScript();

            case "알라딘":
                return  new AladinScript();

            case "Yes24":
                return  new Yes24Script();

            case  "교보문고":
                return  new KyoboScript();

            case "영푼문고":
                return  new YoungPungScript();

            case "리디북스":
                return  new LidiBooksScript();

            case "반디앤루니스":
                return  new BandiScript();

            case "아이허브":
                return new IHerbScript();

            case "아마존" :
                return  new AmazonScript();

            case "알리바바":
                return new AlibabaScript();

            case "라쿠텐":
                return  new RakutenScript();

            case "Qoo10":
                return new Qoo10Script();

            case "10x10":
                return  new TenByTenScript();

            case "이케아":
                return new IkeaScript();

            case "한샘":
                return new HansamScript();

            case "일룸":
                return new IlroomScript();

            case "다이소":
                return new DaisoScript();

            case "하이마트":
                return new HighMartScript();

            case "코스트코":
                return new CostcoScript();

            case "Watsons":
                return new WatonsScript();

            case "ABC마트":
                return new ABCMartScript();

            case "레스모아":
                return new LessmoreScript();

            case "스타일난다":
                return new StylenandaScript();

            case "도라":
                return new DoraScript();

            case "미쳐라":
                return new MichyeraScript();

            case "육육걸즈":
                return new SixSixScript();

            case "불량소녀":
                return new BulangScript();

            case "소녀나라":
                return new GirlNaraScript();

            case "틴블리":
                return new TeenblyScript();

            case "ZARA":
                return new ZaraScript();

            case "H&M":
                return new HMScript();

            case "Mango":
                return new MangoScript();

            case "유니클로" :
                return new UniqloScript();

            case "스파오":
                return new SpaoScript();

            case "8Seconds" :
                return new EightSecondsScript();

            case "멋남":
                return new MutnamScript();

            case "아보키":
                return new AbokiScript();

            case "토모나리":
                return new TomonariScript();

            case "조군샵":
                return new JogunScript();

            case "삼성온라인스토어":
                return new SamsungOnlineScript();

            case "네이버쇼핑":
                return new NaverShoppingScript();

            case "다나와":
                return new DanawaScript();

            case "에누리":
                return new EnuriScript();

            case "우먼스톡":
                return new WomansTalkScript();

            case "미미박스":
                return new MimiBoxScript();

            case "LF몰":
                return new LFMallScript();

            case "이니스프리":
                return new InisfreeScript();

        }

        return  null;
    }

}
