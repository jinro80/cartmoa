package com.chris.cartmoa.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chris.cartmoa.Models.TrackingDetail;
import com.chris.cartmoa.R;
import com.chris.cartmoa.ViewHolders.DeliveryStepViewHolder;

import java.util.List;

/**
 * Created by phj on 2017-01-22.
 */

public class DeliveryStepAdapter extends RecyclerView.Adapter
{
    public List<TrackingDetail> list;

    public DeliveryStepAdapter(List<TrackingDetail> list)
    {
        this.list =list;
    }

    @Override
    public DeliveryStepViewHolder onCreateViewHolder(
            ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.layout_delivery_step_viewholder, viewGroup, false);
        return new DeliveryStepViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        DeliveryStepViewHolder viewHolder = (DeliveryStepViewHolder)holder;
        viewHolder.time.setText(list.get(position).timeString.toString());
        viewHolder.where.setText(list.get(position).where.toString());
        viewHolder.state.setText(list.get(position).kind.toString());
    }



    @Override
    public int getItemCount() {
        return list.size();
    }

}

