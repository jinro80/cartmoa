package com.chris.cartmoa.CartScript;

import com.chris.cartmoa.Models.CartModel;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by phj on 2017-01-24.
 */

public class AlibabaScript implements IWebSrcipt
{
    public CartModel GetCartModel(String response)
    {
        CartModel model = new CartModel();
        model.Market = "알리바바";

        //pname:\s+'(?<image>[^']+)
        Pattern pattern = Pattern.compile("name=\"keywords\"\\s+content=\"([^\"]+)");

        Matcher matcher = pattern.matcher(response);

        if(matcher.groupCount() > 0) {
            while (matcher.find()) {
                model.Title = matcher.group(1).toString();
                break;
            }
        }

        pattern = Pattern.compile("<img\\s+class=\"normal\"\\s+src=\"([^\"]+)");
        matcher = pattern.matcher(response);

        if(matcher.groupCount() > 0) {
            while (matcher.find()) {
                model.ImageUrl = "http:" + matcher.group(1).toString();
                break;
            }

        }

        pattern = Pattern.compile("itemprop=\"lowPrice\">([^<]+)");
        matcher = pattern.matcher(response);

        if(matcher.groupCount() > 0) {
            while (matcher.find()) {
                model.Price = matcher.group(1).toString();
                break;
            }
        }

        return  model;
    }

    public  boolean CanDoCart(String url)
    {

        if(url.toLowerCase().contains("product"))
        {
            return  true;
        }

        return  false;
    }
}
