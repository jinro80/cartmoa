package com.chris.cartmoa.CartScript;

import com.chris.cartmoa.Models.CartModel;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by phj on 2017-01-24.
 */

public class GSShopScript implements IWebSrcipt
{
    public CartModel GetCartModel(String response)
    {
        CartModel model = new CartModel();
        model.Market = "GSSHOP";

        //pname:\s+'(?<image>[^']+)
        Pattern pattern = Pattern.compile("property=\"og:title\"\\s+content=\"([^\"]+)");

        Matcher matcher = pattern.matcher(response);

        if(matcher.groupCount() > 0) {
            while (matcher.find()) {
                model.Title = matcher.group(1).toString();
                break;
            }
        }

        pattern = Pattern.compile("property=\"og:image\"\\s+content=\"([^\"]+)");
        matcher = pattern.matcher(response);

        if(matcher.groupCount() > 0) {
            while (matcher.find()) {
                model.ImageUrl = matcher.group(1).toString();
                break;
            }
        }

        pattern = Pattern.compile("GS가</dfn><strong>([^<]+)");
        matcher = pattern.matcher(response);

        if(matcher.groupCount() > 0) {
            while (matcher.find()) {
                model.Price = matcher.group(1).toString();
                break;
            }
        }

        if(model.Price.equals("0"))
        {
            pattern = Pattern.compile("property=\"recopick:price\"\\s+content=\"([^\"]+)");
            matcher = pattern.matcher(response);

            if(matcher.groupCount() > 0) {
                while (matcher.find()) {
                    model.Price = matcher.group(1).toString();
                    break;
                }
            }
        }

        return  model;
    }

    public  boolean CanDoCart(String url)
    {

        if(url.toLowerCase().contains("dealno=")|| url.toLowerCase().contains("prdid="))
        {
            return  true;
        }

        return  false;
    }
}
