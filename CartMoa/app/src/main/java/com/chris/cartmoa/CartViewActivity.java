package com.chris.cartmoa;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.chris.cartmoa.DB.DBHelper_Cart;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.process.BitmapProcessor;

import org.w3c.dom.Text;

public class CartViewActivity extends AppCompatActivity {

    private TextView registDate;
    private TextView martName;
    private ImageButton editCart;
    private  ImageButton deleteCart;
    private TextView productName;
    private ImageView productImage;
    private TextView productPrice;
    private  ImageButton openBrowser;
    private  ImageButton shareCart;
    private  TextView description;
    ImageLoader imageLoader;
    private DisplayImageOptions options;
    private ImageLoaderConfiguration config;
    DBHelper_Cart db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_view);

        productImage = (ImageView)findViewById(R.id.productImage);
        imageLoader = ImageLoader.getInstance(); // Get singleton instance
        config = ImageLoaderConfiguration.createDefault(this);


        imageLoader.init(config);

        if(!MainActivity.selectedModel.ImageUrl.equals("")) {

            productImage.setBackgroundColor(Color.WHITE);
        }

        imageLoader.displayImage(MainActivity.selectedModel.ImageUrl, productImage);

        db = new DBHelper_Cart(this);

        registDate = (TextView)findViewById(R.id.registDate);
        registDate.setText(MainActivity.selectedModel.RegDate);

        martName = (TextView)findViewById(R.id.martName);
        martName.setText(MainActivity.selectedModel.Market);

        editCart = (ImageButton)findViewById(R.id.editCart);
        editCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditCartView();
            }
        });

        deleteCart = (ImageButton)findViewById(R.id.deleteCart);
        deleteCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                deleteCartItem();

            }
        });

        productName = (TextView)findViewById(R.id.productName);
        productName.setText(MainActivity.selectedModel.Title);


        productPrice = (TextView)findViewById(R.id.productPrice);

        if(MainActivity.selectedModel.Market.equals("알리바바") || MainActivity.selectedModel.Market.equals("아마존") || MainActivity.selectedModel.Market.equals("아이허브") || MainActivity.selectedModel.Market.equals("라쿠텐") || MainActivity.selectedModel.Market.equals("Qoo10")) {


            productPrice.setText( "$" +  MainActivity.selectedModel.Price);
        }
        else {
            productPrice.setText(MainActivity.selectedModel.Price + "원");
        }


        description = (TextView)findViewById(R.id.description);
        description.setText(MainActivity.selectedModel.Description);

        openBrowser = (ImageButton)findViewById(R.id.openBrowser);
        openBrowser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callBrowser();
            }
        });

        shareCart = (ImageButton)findViewById(R.id.shareCart);
        shareCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Share();
            }
        });
    }

    private void SetData()
    {
        if(!MainActivity.selectedModel.ImageUrl.equals("")) {

            productImage.setBackgroundColor(Color.WHITE);
        }

        imageLoader.displayImage(MainActivity.selectedModel.ImageUrl, productImage);

        registDate.setText(MainActivity.selectedModel.RegDate);
        martName.setText(MainActivity.selectedModel.Market);
        productName.setText(MainActivity.selectedModel.Title);

        if(MainActivity.selectedModel.Market.equals("알리바바") || MainActivity.selectedModel.Market.equals("아마존") || MainActivity.selectedModel.Market.equals("아이허브") || MainActivity.selectedModel.Market.equals("라쿠텐") || MainActivity.selectedModel.Market.equals("Qoo10")) {

            productPrice.setText( "$" +  MainActivity.selectedModel.Price);
        }
        else {
            productPrice.setText(MainActivity.selectedModel.Price + "원");
        }

        description.setText(MainActivity.selectedModel.Description);

    }

    public void callBrowser() {
        try {
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(MainActivity.selectedModel.ProductUrl));
            startActivity(i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Share()
    {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, productName.getText());
        intent.putExtra(Intent.EXTRA_TEXT, MainActivity.selectedModel.Market + ", " + MainActivity.selectedModel.Title + ", " + MainActivity.selectedModel.Price + "원, " + MainActivity.selectedModel.ProductUrl);

        Intent chooser = Intent.createChooser(intent, "공유");
        startActivity(chooser);
    }

    private void EditCartView()
    {
        final Intent intent = new Intent(this, EditCartActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        this.startActivityForResult(intent, 500);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK){

            SetData();
        }
    }

    private void deleteCartItem()
    {
        AlertDialog.Builder alert_confirm = new AlertDialog.Builder(this);
        alert_confirm.setMessage("삭제 하시겠습니까?").setCancelable(false).setPositiveButton("예",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {

                            db.DeleteCart(MainActivity.selectedModel.ID);

                            finish();
                        }
                        catch (Exception e)
                        {

                        }
                    }
                }).setNegativeButton("아니오",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // 'No'
                        return;
                    }
                });
        AlertDialog alert = alert_confirm.create();
        alert.show();
    }

    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
