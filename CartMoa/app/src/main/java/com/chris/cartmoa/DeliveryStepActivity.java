package com.chris.cartmoa;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.chris.cartmoa.Adapters.DeliveryStepAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class DeliveryStepActivity extends AppCompatActivity {

    TextView company;
    TextView invoiceNumber;
    TextView comment;
    RecyclerView recycler_view;
    DeliveryStepAdapter adapter;
    TextView emptyTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_step);

        AdView mAdView = (AdView)findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("CFD62DC52635837D93B1BB6949AA1A6A").build();
        mAdView.loadAd(adRequest);


        recycler_view = (RecyclerView)findViewById(R.id.recycler_view);


        adapter = new DeliveryStepAdapter(MainController.Info.trackingDetails);
        recycler_view.setAdapter(adapter);
        recycler_view.setLayoutManager(new LinearLayoutManager(this));

        emptyTitle = (TextView)findViewById(R.id.emptyTitle);

        company = (TextView)findViewById(R.id.company);
        invoiceNumber = (TextView)findViewById(R.id.invoiceNumber);
        comment = (TextView)findViewById(R.id.comment);

        if(MainController.Info !=null)
        {
            company.setText(MainController.Info.DeliveryCompany);
            invoiceNumber.setText(MainController.Info.invoiceNo);
            comment.setText(MainController.Info.itemName);


            if(MainController.Info.trackingDetails.size() < 1)
            {
                recycler_view.setVisibility(View.GONE);
                emptyTitle.setVisibility(View.VISIBLE);
            }
        }


    }

    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
