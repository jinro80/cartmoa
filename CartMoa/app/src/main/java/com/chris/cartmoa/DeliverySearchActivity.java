package com.chris.cartmoa;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.chris.cartmoa.Adapters.DBHistoryAdapter;
import com.chris.cartmoa.DB.DBHelper_Delivery;
import com.chris.cartmoa.Models.DeliveryHistory;
import com.chris.cartmoa.Models.DeliveryInfo;
import com.chris.cartmoa.Models.TrackingDetail;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson2.JacksonFactory;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DeliverySearchActivity extends AppCompatActivity {

    static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    static final JsonFactory JSON_FACTORY = new JacksonFactory();
    ArrayAdapter<CharSequence> adspin;
    String deliveryCompany = "CJ대한통운";
    EditText invoiceNumber;
    EditText comment;
    String deliveryCompanyCode = "04";
    Spinner spinner;
    ProgressDialog mProgressDialog = null;

    DBHelper_Delivery db;

    DBHistoryAdapter adapter;
    RecyclerView recycler_view;
    List<DeliveryHistory> history;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_search);

        db = new DBHelper_Delivery(this);

        invoiceNumber = (EditText)findViewById(R.id.invoiceNumber);
        comment = (EditText)findViewById(R.id.comment);

    /*    AdView mAdView = (AdView)view.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("CFD62DC52635837D93B1BB6949AA1A6A").build();
        mAdView.loadAd(adRequest);*/

        spinner = (Spinner)findViewById(R.id.spinner);
        spinner.setPrompt("택배사를 선택하세요");

        adspin = ArrayAdapter.createFromResource(this, R.array.selected,    android.R.layout.simple_spinner_item);

        adspin.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adspin);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?>  parent, View view, int position, long id) {
              /*  Toast.makeText(getActivity(),
                        adspin.getItem(position) + "을 선택 했습니다.", 1).show();*/

                deliveryCompany = adspin.getItem(position).toString();
                deliveryCompanyCode = GetDeliveryCompanyCode(deliveryCompany);
            }
            public void onNothingSelected(AdapterView<?>  parent) {
            }
        });



        Button registerDelivery = (Button)findViewById(R.id.registerDelivery);
        registerDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RegisterDelivery();
            }
        });

        recycler_view = (RecyclerView) findViewById(R.id.recycler_view);

        history =  db.GetAllDeliveryHistory();

        adapter = new DBHistoryAdapter(history, this);

        recycler_view.setAdapter(adapter);
        recycler_view.setLayoutManager(new LinearLayoutManager(this));



    }

    private void RegisterDelivery()
    {
        if(invoiceNumber.getText().length() < 1) {

            ShowError("송장 번호를 입력하세요.");
            return;
        }

        mProgressDialog = ProgressDialog.show(this,"",
                "등록중입니다.",true);

        new Thread() {
            public void run() {


                try {
                    HttpRequestFactory requestFactory = HTTP_TRANSPORT
                            .createRequestFactory(new HttpRequestInitializer() {
                                @Override
                                public void initialize(HttpRequest request) {
                                    request.setParser(new JsonObjectParser(JSON_FACTORY));
                                }
                            });

                            /*String t_code ="08";
                            String t_invoice = "224871113692";
                            String url = String.format("https://m.search.naver.com/p/csearch/content/util/headerjson.nhn?_callback=window.__jindo2_callback._3164&callapi=parceltracking&t_code=%s&t_invoice=%s&", t_code, t_invoice);*/
                    String url = String.format("https://m.search.naver.com/p/csearch/content/util/headerjson.nhn?_callback=window.__jindo2_callback._3164&callapi=parceltracking&t_code=%s&t_invoice=%s&", deliveryCompanyCode, invoiceNumber.getText());

                    HttpRequest request = requestFactory.buildGetRequest(new GenericUrl(url));

                    String reulst = request.executeAsync().get().parseAsString();

                    reulst =  reulst.replace("window.__jindo2_callback._3164(", "");
                    reulst = reulst.replace(");", "");

                    if (reulst.length() > 0) {
                        JSONObject object = new JSONObject(reulst);

                        final  DeliveryInfo info = new DeliveryInfo();
                        info.complete = object.getBoolean("complete");
                        info.invoiceNo = object.getString("invoiceNo");
                        info.itemName = comment.getText().toString();
                        info.level = object.getString("level");
                        info.DeliveryCompany = deliveryCompany;

                        List<TrackingDetail> list = new ArrayList<TrackingDetail>();
                        JSONArray jArray = object.getJSONArray("trackingDetails");

                        for(int i = 0; i < jArray.length(); i++)
                        {
                            JSONObject jObject = jArray.getJSONObject(i);

                            TrackingDetail detail = new TrackingDetail();
                            detail.kind = jObject.getString("kind");
                            detail.where = jObject.getString("where");
                            detail.timeString = jObject.getString("timeString");
                            detail.manName = jObject.getString("manName");
                            detail.telno = jObject.getString("telno");
                            detail.telno2 = jObject.getString("telno2");

                            list.add(detail);
                        }


                        info.trackingDetails = list;
                        MainController.Info = info;

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                db.DeleteDeliveryInfo(invoiceNumber.getText().toString());


                                DeliveryHistory deliveryHistory = new DeliveryHistory();
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

                                deliveryHistory.RegDate = simpleDateFormat.format(new Date());
                                deliveryHistory.Company = deliveryCompany;
                                deliveryHistory.CompanyCode = deliveryCompanyCode;
                                deliveryHistory.Complete = String.valueOf(info.complete);
                                deliveryHistory.InvoceNumber = invoiceNumber.getText().toString();
                                deliveryHistory.Comment = comment.getText().toString();
                                deliveryHistory.Level = info.level;
                                db.AddDeliveryHistory(deliveryHistory);

                                     /*   List<DeliveryHistory> history = db.GetAllDeliveryHistory();

                                        for(int j=0 ; j < history.size(); j ++ )
                                        {
                                            int k = history.size();
                                        }*/

                                if(mProgressDialog != null && mProgressDialog.isShowing())
                                {
                                    mProgressDialog.dismiss();
                                }

                                Clear();
                                ShowDeliveryStepView();

                                adapter.Refresh();
                            }
                        });



                    } else {

                        ShowError("택배 정보 혹은 인터넷 상태를 확인해주세요.");
                    }
                }
                catch (Exception e)
                {
                    Log.d("Exception: ", e.toString());
                    MainController.Info = null;

                    ShowError("택배 정보 혹은 인터넷 상태를 확인해주세요.");

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                                mProgressDialog.dismiss();
                            }
                        }
                    });
                }

            }
        }.start();
    }


    private void ShowError(String msg)
    {
        Toast.makeText(this,
                msg , Toast.LENGTH_LONG).show();

    }

    private  void Clear()
    {
        invoiceNumber.setText("");
        comment.setText("");
        deliveryCompanyCode = "04";
        deliveryCompany = "CJ대한통운";
        spinner.setSelection(0);
    }

    private String GetDeliveryCompanyCode(String name)
    {
        switch (name)
        {
            case "CJ대한통운" : return "04";
            case "우체국택배" : return "01";
            case "한진택배" : return "05";
            case "롯데택배" : return "08";
            case "로젠택배" : return "06";
            case "KG로지스" : return "39";
            case "CVSnet 편의점택배" : return "24";
            case "KGB택배" : return "10";
            case "경동택배" : return "23";
            case "대신택배" : return "22";
            case "일양로지스" : return "11";
            case "합동택배" : return "32";
            case "GTX로지스" : return "15";
            case "건영택배" : return "18";
            case "천일택배" : return "17";
            case "한의사랑택배" : return "16";
            case "한덱스" : return "20";
            case "EMS" : return "12";
            case "DHL" : return "13";
            case "TNT Express" : return "25";
            case "UPS" : return "14";
            case "Fedex" : return "21";
            case "USPS" : return "26";
            case "i-Parcel" : return "34";
            case "DHL Global Mail" : return "33";
            case "범한판토스" : return "37";
            case "에어보이익스프레스" : return "29";
            case "GSMNtoN" : return "28";
            case "APEX(ECMS Express)" : return "38";
            case "KGL네트웍스" : return "30";
            case "굿투럭" : return "40";
            case "호남택배" : return "45";

            default: return  "";
        }
    }


    private  void ShowDeliveryStepView()
    {
        final Intent intent = new Intent(this, DeliveryStepActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
