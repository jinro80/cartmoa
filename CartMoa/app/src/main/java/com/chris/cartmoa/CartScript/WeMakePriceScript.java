package com.chris.cartmoa.CartScript;

import com.chris.cartmoa.Models.CartModel;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by phj on 2017-01-24.
 */

public class WeMakePriceScript implements IWebSrcipt
{
    public CartModel GetCartModel(String response)
    {
        CartModel model = new CartModel();
        model.Market = "위메프";

        //pname:\s+'(?<image>[^']+)
        Pattern pattern = Pattern.compile("id=\"main_name\">([^<]+)");

        Matcher matcher = pattern.matcher(response);

        if(matcher.groupCount() > 0) {
            while (matcher.find()) {
                model.Title = matcher.group(1).toString();
                break;
            }
        }

        pattern = Pattern.compile("<img\\s+id=\"img_app_onecut\"\\s+src=\"([^\"]+)");
        matcher = pattern.matcher(response);

        if(matcher.groupCount() > 0) {
            while (matcher.find()) {
                model.ImageUrl = matcher.group(1).toString();
                break;

            }
        }

        pattern = Pattern.compile("none_origin\"><strong>([^<]+)");
        matcher = pattern.matcher(response);

        if(matcher.groupCount() > 0) {
            while (matcher.find()) {
                model.Price = matcher.group(1).toString();
                break;
            }
        }

        if(model.Price.equals("0"))
        {
            pattern = Pattern.compile("class=\"sale\"><strong>([^<]+)");
            matcher = pattern.matcher(response);

            while (matcher.find()) {
                model.Price = matcher.group(1).toString();
                break;
            }
        }

        return  model;
    }

    public  boolean CanDoCart(String url)
    {

        if(url.toLowerCase().contains("deal/adeal"))
        {
            return  true;
        }

        return  false;
    }
}
