package com.chris.cartmoa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.chris.cartmoa.DB.DBHelper_Delivery;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        try
        {
        DBHelper_Delivery db = new DBHelper_Delivery(this );
        db.getWritableDatabase();

            if(db.testDB()) {
                final Intent intent = new Intent(this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);

                finish();
            }
            else
            {
                finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
