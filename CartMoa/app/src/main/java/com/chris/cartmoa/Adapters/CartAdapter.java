package com.chris.cartmoa.Adapters;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chris.cartmoa.CartViewActivity;
import com.chris.cartmoa.DB.DBHelper_Cart;
import com.chris.cartmoa.EditCartActivity;
import com.chris.cartmoa.MainActivity;
import com.chris.cartmoa.Models.CartModel;
import com.chris.cartmoa.R;
import com.chris.cartmoa.ViewHolders.CartViewHolder;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.process.BitmapProcessor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by phj on 2017-01-19.
 */

public class CartAdapter extends RecyclerView.Adapter {

    public List<CartModel> list;
    DBHelper_Cart db;
    Activity context;
    ImageLoader imageLoader;
    private DisplayImageOptions options;
    private ImageLoaderConfiguration config;


    public CartAdapter(Activity context)
    {
        this.context = context;
        this.list = new ArrayList<CartModel>();
        db = new DBHelper_Cart(context);

        list = db.GetAllCart();

        imageLoader = ImageLoader.getInstance(); // Get singleton instance
        config = ImageLoaderConfiguration.createDefault(context);
        imageLoader.init(config);
    }


    public void Clear()
    {
        list.clear();
        notifyDataSetChanged();
    }

    public void refreshData(String sort)
    {

        list.clear();
        list = db.GetAllCart(sort);
        notifyDataSetChanged();

        if(((MainActivity) context).mSwipeRefresh != null)
        {
            ((MainActivity) context).mSwipeRefresh.setRefreshing(false);
        }
    }

    public  void searchData(String keyword)
    {
        list.clear();
        list = db.GetCart(keyword);
        notifyDataSetChanged();
    }

    public void refreshData()
    {
        list.clear();
        list = db.GetAllCart();
        notifyDataSetChanged();
    }

    @Override
    public CartViewHolder onCreateViewHolder(
            ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.cartviewholder, viewGroup, false);
        return new CartViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final CartViewHolder viewHolder = (CartViewHolder)holder;

        if(options == null)
        {
            options = new DisplayImageOptions.Builder()
                    .imageScaleType(ImageScaleType.EXACTLY)
                    .resetViewBeforeLoading(true)
                    .showImageForEmptyUri(R.drawable.ic_image_white_48dp)
                    .showImageOnFail(R.drawable.ic_image_white_48dp)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(false)
                    .postProcessor(new BitmapProcessor() {
                        @Override
                        public Bitmap process(Bitmap bmp) {
                            return Bitmap.createScaledBitmap(bmp, viewHolder.prouctImage.getWidth(), viewHolder.prouctImage.getHeight(), false);
                        }
                    })
                    .build();
        }

        viewHolder.description.setText(list.get(position).Description);
        viewHolder.registDate.setText(list.get(position).RegDate);
        viewHolder.productName.setText(list.get(position).Title);
        viewHolder.martName.setText(list.get(position).Market);

        if(list.get(position).Market.equals("알리바바") || list.get(position).Market.equals("아마존") || list.get(position).Market.equals("아이허브") || list.get(position).Market.equals("라쿠텐") || list.get(position).Market.equals("Qoo10")) {


            viewHolder.productPrice.setText( "$" +  list.get(position).Price);
        }
        else {
            viewHolder.productPrice.setText(list.get(position).Price + "원");
        }

        if(list.get(position).ImageUrl.toString().equals(""))
        {
            viewHolder.prouctImage.setBackgroundColor(Color.LTGRAY);
            viewHolder.prouctImage.setImageResource(R.drawable.ic_image_white_48dp);
        }
        else {

            Uri selectedImageUri = Uri.parse(list.get(position).ImageUrl.toString());
            imageLoader.displayImage(selectedImageUri.toString(), viewHolder.prouctImage);
            viewHolder.prouctImage.setBackgroundColor(Color.WHITE);

     /*       imageLoader.displayImage(selectedImageUri.toString(), viewHolder.prouctImage, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {

                    String aaa =imageUri;
                }
                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    String aaa =imageUri;
                }
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    String aaa =imageUri;
                }
                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                    String aaa =imageUri;
                }
            }, new ImageLoadingProgressListener() {
                @Override
                public void onProgressUpdate(String imageUri, View view, int current, int total) {
                    String aaa =imageUri;
                }
            });
            //*/
        }

        viewHolder.rootLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //상세보기 Activity.

                MainActivity.selectedModel = list.get(position);

                ShowCartView();
            }
        });

        viewHolder.deleteCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AlertDialog.Builder alert_confirm = new AlertDialog.Builder(context);
                alert_confirm.setMessage("삭제 하시겠습니까?").setCancelable(false).setPositiveButton("예",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {

                                    db.DeleteCart(list.get(position).ID);
                                    list.clear();
                                    list = db.GetAllCart();
                                    notifyDataSetChanged();
                                }
                                catch (Exception e)
                                {

                                }
                            }
                        }).setNegativeButton("아니오",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // 'No'
                                return;
                            }
                        });
                AlertDialog alert = alert_confirm.create();
                alert.show();
            }
        });

    }

    private void ShowCartView()
    {
        final Intent intent = new Intent(context, CartViewActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        context.startActivityForResult(intent, 500);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

}