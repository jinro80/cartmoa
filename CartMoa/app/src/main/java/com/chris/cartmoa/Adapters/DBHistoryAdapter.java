package com.chris.cartmoa.Adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.chris.cartmoa.DB.DBHelper_Delivery;
import com.chris.cartmoa.DeliveryStepActivity;
import com.chris.cartmoa.MainController;
import com.chris.cartmoa.Models.DeliveryHistory;
import com.chris.cartmoa.Models.DeliveryInfo;
import com.chris.cartmoa.Models.TrackingDetail;
import com.chris.cartmoa.R;
import com.chris.cartmoa.ViewHolders.DeliveryHistoryViewHolder;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson2.JacksonFactory;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.widget.Toast.LENGTH_LONG;

/**
 * Created by phj on 2017-01-22.
 */

public class DBHistoryAdapter extends RecyclerView.Adapter
{
    static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    static final JsonFactory JSON_FACTORY = new JacksonFactory();
    DBHelper_Delivery db;
    ProgressDialog mProgressDialog = null;

    public List<DeliveryHistory> list;

    Activity context;

    public void Refresh()
    {
        list.clear();
        list = db.GetAllDeliveryHistory();
        notifyDataSetChanged();
    }

    public DBHistoryAdapter(List<DeliveryHistory> list, Activity context)
    {
        this.context = context;
        this.list =list;
        db = new DBHelper_Delivery(context);
    }

    @Override
    public DeliveryHistoryViewHolder onCreateViewHolder(
            ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.delivery_history_viewholder, viewGroup, false);
        return new DeliveryHistoryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final DeliveryHistoryViewHolder viewHolder = (DeliveryHistoryViewHolder)holder;

        viewHolder.comment.setText(list.get(position).Comment);
        viewHolder.registDate.setText(list.get(position).RegDate);
        viewHolder.invoceNumber.setText(list.get(position).InvoceNumber);
        viewHolder.selectDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mProgressDialog = ProgressDialog.show(context,"",
                        "조회 중입니다..",true);


                new Thread() {
                    public void run() {


                        try {
                            HttpRequestFactory requestFactory = HTTP_TRANSPORT
                                    .createRequestFactory(new HttpRequestInitializer() {
                                        @Override
                                        public void initialize(HttpRequest request) {
                                            request.setParser(new JsonObjectParser(JSON_FACTORY));
                                        }
                                    });


                            String url = String.format("https://m.search.naver.com/p/csearch/content/util/headerjson.nhn?_callback=window.__jindo2_callback._3164&callapi=parceltracking&t_code=%s&t_invoice=%s&", list.get(position).CompanyCode, list.get(position).InvoceNumber);

                            HttpRequest request = requestFactory.buildGetRequest(new GenericUrl(url));

                            String reulst = request.executeAsync().get().parseAsString();

                            reulst =  reulst.replace("window.__jindo2_callback._3164(", "");
                            reulst = reulst.replace(");", "");

                            if (reulst.length() > 0) {
                                JSONObject object = new JSONObject(reulst);

                                final  DeliveryInfo info = new DeliveryInfo();
                                info.complete = object.getBoolean("complete");
                                info.invoiceNo = object.getString("invoiceNo");
                                info.itemName = list.get(position).Comment;
                                info.DeliveryCompany = list.get(position).Company;
                                info.level = object.getString("level");

                                final List<TrackingDetail> detailList = new ArrayList<TrackingDetail>();
                                JSONArray jArray = object.getJSONArray("trackingDetails");

                                for(int i = 0; i < jArray.length(); i++)
                                {
                                    JSONObject jObject = jArray.getJSONObject(i);

                                    TrackingDetail detail = new TrackingDetail();
                                    detail.kind = jObject.getString("kind");
                                    detail.where = jObject.getString("where");
                                    detail.timeString = jObject.getString("timeString");
                                    detail.manName = jObject.getString("manName");
                                    detail.telno = jObject.getString("telno");
                                    detail.telno2 = jObject.getString("telno2");

                                    detailList.add(detail);
                                }


                                info.trackingDetails = detailList;
                                MainController.Info = info;




                                context.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        db.DeleteDeliveryInfo(list.get(position).InvoceNumber);


                                        DeliveryHistory deliveryHistory = new DeliveryHistory();
                                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

                                        deliveryHistory.RegDate = simpleDateFormat.format(new Date());
                                        deliveryHistory.Company = list.get(position).Company;
                                        deliveryHistory.CompanyCode = list.get(position).CompanyCode;
                                        deliveryHistory.Complete = String.valueOf(info.complete);
                                        deliveryHistory.InvoceNumber = list.get(position).InvoceNumber;
                                        deliveryHistory.Comment = list.get(position).Comment;
                                        deliveryHistory.Level = info.level;
                                        db.AddDeliveryHistory(deliveryHistory);



                                        if (mProgressDialog != null && mProgressDialog.isShowing()) {
                                            mProgressDialog.dismiss();
                                        }

                                        ShowDeliveryStepView();
                                    }
                                });

                            } else {

                                Toast.makeText(context,
                                        "택배 정보 혹은 인터넷 상태를 확인해주세요.", LENGTH_LONG).show();
                            }
                        }
                        catch (Exception e)
                        {
                            Log.d("Exception: ", e.toString());
                            MainController.Info = null;

                            Toast.makeText(context,
                                    "택배 정보 혹은 인터넷 상태를 확인해주세요.", LENGTH_LONG).show();

                            context.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    if (mProgressDialog != null && mProgressDialog.isShowing()) {
                                        mProgressDialog.dismiss();
                                    }
                                }
                            });


                        }

                    }
                }.start();

            }
        });

        viewHolder.deleteHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AlertDialog.Builder alert_confirm = new AlertDialog.Builder(context);
                alert_confirm.setMessage("삭제 하시겠습니까?").setCancelable(false).setPositiveButton("예",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {

                                    db.DeleteDeliveryInfo(list.get(position).InvoceNumber);
                                    list = db.GetAllDeliveryHistory();
                                    notifyDataSetChanged();
                                }
                                catch (Exception e)
                                {

                                }
                            }
                        }).setNegativeButton("아니오",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // 'No'
                                return;
                            }
                        });
                AlertDialog alert = alert_confirm.create();
                alert.show();
            }
        });

    }

    private  void ShowDeliveryStepView()
    {
        final Intent intent = new Intent(context, DeliveryStepActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        context.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}
