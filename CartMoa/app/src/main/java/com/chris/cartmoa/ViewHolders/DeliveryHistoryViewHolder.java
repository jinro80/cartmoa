package com.chris.cartmoa.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.chris.cartmoa.R;

/**
 * Created by phj on 2017-01-22.
 */

public class DeliveryHistoryViewHolder extends RecyclerView.ViewHolder
{
    public TextView registDate;
    public TextView invoceNumber;
    public TextView comment;
    public Button selectDelivery;
    public Button deleteHistory;

    public DeliveryHistoryViewHolder(View itemView){
        super(itemView);

        registDate = (TextView)itemView.findViewById(R.id.registDate);
        invoceNumber = (TextView)itemView.findViewById(R.id.invoceNumber);
        comment = (TextView)itemView.findViewById(R.id.comment);

        selectDelivery = (Button)itemView.findViewById(R.id.selectDelivery);
        deleteHistory = (Button)itemView.findViewById(R.id.deleteHistory);

    }
}