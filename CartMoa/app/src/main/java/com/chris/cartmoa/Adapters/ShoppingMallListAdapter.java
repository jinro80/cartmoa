package com.chris.cartmoa.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chris.cartmoa.Models.ShoppingMall;
import com.chris.cartmoa.R;
import com.chris.cartmoa.ShoppingActivity;
import com.chris.cartmoa.ViewHolders.ShoppingMallListViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by phj on 2017-01-19.
 */

public class ShoppingMallListAdapter extends RecyclerView.Adapter {

    public List<ShoppingMall> list;
    String marketName = "";
    Activity context;


    public ShoppingMallListAdapter(Activity context)
    {
        this.context = context;
        this.list = new ArrayList<ShoppingMall>();

        SetAllShoppingMall();

    }

    public void SetCompare()
    {
        list.clear();

        list.add(NaverShopping());
        list.add(Danawa());
        list.add(Enuri());

        this.notifyDataSetChanged();

    }

    public void SetDigital()
    {
        list.clear();

        list.add(HiMart());
        list.add(SamsungOnlineStore());

        this.notifyDataSetChanged();

    }

    public void SetHomshopping()
    {
        list.clear();

        list.add(NSHomeShopping());
        list.add(HomeAndShopping());
        list.add(KShopping());
        list.add(LotteIMall());
        this.notifyDataSetChanged();
    }

    public void SetCulture()
    {
        list.clear();

        list.add(Yes24());
        list.add(Aladin());
        list.add(Kyobo());
        list.add(YoungPung());
        list.add(RidiBooks());
        list.add(Bandi());
        this.notifyDataSetChanged();
    }


    public void SetHome()
    {
        list.clear();

        list.add(TenByTen());
        list.add(Ikea());
        list.add(Hansam());
        list.add(ILRoom());
        list.add(Daiso());

        this.notifyDataSetChanged();
    }


    public void SetOverSea()
    {
        list.clear();

        list.add(Amazon());
        list.add(Alibaba());
        list.add(Ihreb());
        list.add(Rakuten());
        list.add(Qoo10());

        this.notifyDataSetChanged();
    }

    public void SetBeauty()
    {
        list.clear();

        list.add(OliveYoung()); //올리브영
        list.add(Watsons());
        list.add(WomanStalk());
        list.add(MimiBox());
        list.add(Inisfree());

        this.notifyDataSetChanged();
    }

    public void SetMale()
    {
        list.clear();

        list.add(Mutnam()); //멋남
        list.add(Aboki()); //아보키
        list.add(Tomonari()); //토모나리
        list.add(JogunShop()); //조군샵

        this.notifyDataSetChanged();
    }

    public void SetFemale()
    {
        list.clear();

        list.add(Stylenanda()); //스타일난다
        list.add(Dora()); //도라
        list.add(Michyeora()); //미쳐라
        list.add(SixSixGirls()); //육육걸즈
        list.add(Bullang()); //불량소녀
        list.add(Sonyunara()); //소녀나라
        list.add(Teenbly()); //틴블리

        this.notifyDataSetChanged();
    }

    public void SetFassion()
    {
        list.clear();

        //list.add(ABC()); //ABC 마트
        list.add(Lesmore()); //레스모아
        list.add(Zara()); //ZARA
        list.add(HAndM()); //H&M
        list.add(Mango()); //망고
        list.add(Uniqlo()); //유니클로
        list.add(Spao()); //스파오
        list.add(EightSconds()); //에잇세컨즈
        list.add(LFMall()); //LF Mall

        this.notifyDataSetChanged();
    }

    public  void SetTotal()
    {
        list.clear();

        list.add(Gmarket()); //G마켓
        list.add(ElevenST()); //11번가
        list.add(Auction()); //옥션
        list.add(InterPark()); //인터파크
        list.add(Coupang()); //쿠팡
        list.add(WemakePrice()); //위메프
        list.add(Tmon()); //티몬
        list.add(EMART()); //이마트
        list.add(HomePlus()); //홈플러스
        list.add(LotteCom()); // 롯데닷컴
        list.add(LotteMart()); //롯데마트
        list.add(HMall()); // H몰
        list.add(GSSHOP()); //GSSHOP
        list.add(SSG()); //SSG
        list.add(G9()); //G9
        list.add(Hyundai()); //현대백화점몰
        list.add(CJMall());//CJ몰
        list.add(AKMall()); //AK몰
        list.add(Costco()); //코스트코

        notifyDataSetChanged();
    }

    public void SetAllShoppingMall()
    {
        list.clear();

        list.add(Gmarket()); //G마켓
        list.add(ElevenST()); //11번가
        list.add(Auction()); //옥션
        list.add(InterPark()); //인터파크
        list.add(Coupang()); //쿠팡
        list.add(WemakePrice()); //위메프
        list.add(Tmon()); //티몬
        list.add(EMART()); //이마트
        list.add(HomePlus()); //홈플러스
        list.add(LotteIMall()); //롯데홈쇼핑
        list.add(LotteCom()); // 롯데닷컴
        list.add(LotteMart()); //롯데마트
        list.add(HMall()); // H몰
        list.add(GSSHOP()); //GSSHOP
        list.add(SSG()); //SSG
        list.add(G9()); //G9
        list.add(Hyundai()); //현대백화점몰
        list.add(CJMall());//CJ몰
        list.add(AKMall()); //AK몰
        list.add(OliveYoung()); //올리브영
        list.add(NSHomeShopping()); //NS HomeShopping
        list.add(HomeAndShopping()); //홈앤쇼핑
        list.add(KShopping()); //K쇼핑
        list.add(Aladin()); //알라딘
        list.add(Yes24()); //yes24
        list.add(Kyobo()); //교보문고
        list.add(YoungPung()); //영풍문고
        list.add(RidiBooks()); //리디북스
        list.add(Bandi()); //반디앤루니스
        list.add(Ihreb()); //아이허브
        list.add(Amazon()); //아마존
        list.add(Alibaba()); //알리바바
        list.add(Rakuten()); //라쿠텐
        list.add(Qoo10()); //Qoo10
        list.add(TenByTen()); //10x10
        list.add(Ikea()); //이케아
        list.add(Hansam()); //한샘
        list.add(ILRoom()); //일룸
        list.add(Daiso()); //다이소
        list.add(HiMart()); //하이마트
        list.add(Costco()); //코스트코
        list.add(Watsons()); //왓슨스
        //list.add(ABC()); //ABC 마트
        list.add(Lesmore()); //레스모아
        list.add(Stylenanda()); //스타일난다
        list.add(Dora()); //도라
        list.add(Michyeora()); //미쳐라
        list.add(SixSixGirls()); //육육걸즈
        list.add(Bullang()); //불량소녀
        list.add(Sonyunara()); //소녀나라
        list.add(Teenbly()); //틴블리
        list.add(Zara()); //ZARA
        list.add(HAndM()); //H&M
        list.add(Mango()); //망고
        list.add(Uniqlo()); //유니클로
        list.add(Spao()); //스파오
        list.add(EightSconds()); //에잇세컨즈
        list.add(Mutnam()); //멋남
        list.add(Aboki()); //아보키
        list.add(Tomonari()); //토모나리
        list.add(JogunShop()); //조군샵
        list.add(SamsungOnlineStore());//삼성온라인스토어
        list.add(WomanStalk()); //우먼스톡
        list.add(MimiBox()); //미미박스
        list.add(Inisfree()); //이니스프리
        list.add(LFMall()); //LF Mall
        list.add(Enuri()); //에누리
        list.add(NaverShopping()); //네이버 쇼핑
        list.add(Danawa()); //다나와

        this.notifyDataSetChanged();
    }

    @Override
    public ShoppingMallListViewHolder onCreateViewHolder(
            ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.shoppingmalllist_viewholder, viewGroup, false);
        return new ShoppingMallListViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final ShoppingMallListViewHolder viewHolder = (ShoppingMallListViewHolder)holder;

        viewHolder.marketName.setText(list.get(position).Name);

        viewHolder.goShopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //marketName = list.get(position).Name;
                GoShopping(list.get(position).Url, list.get(position).Name);
            }
        });


        viewHolder.marketIcon.setImageResource(list.get(position).Icon);



        viewHolder.rootLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //marketName = list.get(position).Name;
                GoShopping(list.get(position).Url, list.get(position).Name);

            }
        });


    }

    private void GoShopping(String url, String market)
    {
        final Intent intent = new Intent(context, ShoppingActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.putExtra("url", url);
        intent.putExtra("market", market);
        context.startActivityForResult(intent, 500);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

   private ShoppingMall Gmarket()
   {
       ShoppingMall market = new ShoppingMall();
       market.Name = "G마켓";
       market.ID = "1001";
       market.Url = "http://mobile.gmarket.co.kr/";
       market.Type = "종합";
       market.Icon = R.drawable.gmarket;

       return  market;
   }

    private ShoppingMall ElevenST()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "11번가";
        market.ID = "1010";
        market.Url = "http://m.11st.co.kr/";
        market.Type = "종합";
        market.Icon = R.drawable.st11;

        return  market;
    }

    private ShoppingMall Auction()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "옥션";
        market.ID = "1020";
        market.Url = "http://m.auction.co.kr/";
        market.Type = "종합";
        market.Icon = R.drawable.aution;

        return  market;
    }

    private ShoppingMall InterPark()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "인터파크";
        market.ID = "1030";
        market.Url = "http://m.interpark.com/";
        market.Type = "종합";
        market.Icon = R.drawable.interpark;

        return  market;
    }

    private ShoppingMall LotteIMall()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "롯데홈쇼핑";
        market.ID = "1040";
        market.Url = "http://m.lotteimall.com/";
        market.Type = "홈쇼핑";
        market.Icon = R.drawable.lottehomeshopping;

        return  market;
    }

    private ShoppingMall LotteCom()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "롯데닷컴";
        market.ID = "1050";
        market.Url = "http://m.lotte.com/";
        market.Type = "종합";
        market.Icon = R.drawable.lottecom;

        return  market;
    }

    private ShoppingMall LotteMart()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "롯데마트";
        market.ID = "1060";
        market.Url = "http://m.lottemart.com/";
        market.Type = "종합";
        market.Icon = R.drawable.lottemart;

        return  market;
    }

    private ShoppingMall HMall()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "현대H몰";
        market.ID = "1070";
        market.Url = "http://m.hyundaihmall.com/";
        market.Type = "종합";
        market.Icon = R.drawable.hmall;

        return  market;
    }

    private ShoppingMall GSSHOP()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "GSSHOP";
        market.ID = "1080";
        market.Url = "http://m.gsshop.com/";
        market.Type = "종합";
        market.Icon = R.drawable.gsshop;

        return  market;
    }

    private ShoppingMall EMART()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "이마트몰";
        market.ID = "1090";
        market.Url = "http://m.emart.ssg.com/";
        market.Type = "종합";
        market.Icon = R.drawable.emart;

        return  market;
    }

    private ShoppingMall HomePlus()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "홈플러스";
        market.ID = "1100";
        market.Url = "http://m.homeplus.co.kr/";
        market.Type = "종합";
        market.Icon = R.drawable.homeplus;

        return  market;
    }

    private ShoppingMall SSG()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "SSG";
        market.ID = "1110";
        market.Url = "http://m.shinsegaemall.ssg.com/";
        market.Type = "종합";
        market.Icon = R.drawable.ssg;

        return  market;
    }

    private ShoppingMall G9()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "G9";
        market.ID = "1120";
        market.Url = "http://m.g9.co.kr/";
        market.Type = "종합";
        market.Icon = R.drawable.g9;

        return  market;
    }

    private ShoppingMall Hyundai()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "더현대닷컴";
        market.ID = "1130";
        market.Url = "http://m.thehyundai.com/";
        market.Type = "종합";
        market.Icon = R.drawable.hyundai;

        return  market;
    }

    private ShoppingMall CJMall()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "CJ몰";
        market.ID = "1140";
        market.Url = "http://mw.cjmall.com/";
        market.Type = "종합";
        market.Icon = R.drawable.cjmall;

        return  market;
    }

    private ShoppingMall AKMall()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "AK몰";
        market.ID = "1150";
        market.Url = "http://m.akmall.com/";
        market.Type = "종합";
        market.Icon = R.drawable.akmall;

        return  market;
    }

    private ShoppingMall OliveYoung()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "올리브영";
        market.ID = "1160";
        market.Url = "http://mw.oliveyoungshop.com/";
        market.Type = "뷰티";
        market.Icon = R.drawable.olive;

        return  market;
    }

    private ShoppingMall Coupang()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "쿠팡";
        market.ID = "1170";
        market.Url = "http://m.coupang.com/";
        market.Type = "종합";
        market.Icon = R.drawable.coupang;

        return  market;
    }

    private ShoppingMall WemakePrice()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "위메프";
        market.ID = "1180";
        market.Url = "http://m.wemakeprice.com/";
        market.Type = "종합";
        market.Icon = R.drawable.wemakeprice;

        return  market;
    }

    private ShoppingMall Tmon()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "티몬";
        market.ID = "1190";
        market.Url = "http://m.ticketmonster.co.kr/";
        market.Type = "종합";
        market.Icon = R.drawable.tmon;

        return  market;
    }

    private ShoppingMall NSHomeShopping()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "NS홈쇼핑";
        market.ID = "1200";
        market.Url = "http://m.nsmall.com/";
        market.Type = "홈쇼핑";
        market.Icon = R.drawable.nshome;

        return  market;
    }

    private ShoppingMall HomeAndShopping()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "홈앤쇼핑";
        market.ID = "1210";
        market.Url = "http://m.hnsmall.com/";
        market.Type = "홈쇼핑";
        market.Icon = R.drawable.homeandshopping;

        return  market;
    }

    private ShoppingMall KShopping()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "K쇼핑";
        market.ID = "1220";
        market.Url = "http://m.kshop.co.kr/";
        market.Type = "홈쇼핑";
        market.Icon = R.drawable.kshopping;

        return  market;
    }

    private ShoppingMall Aladin()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "알라딘";
        market.ID = "1230";
        market.Url = "http://m.aladin.co.kr/";
        market.Type = "문화";
        market.Icon = R.drawable.aladin;

        return  market;
    }

    private ShoppingMall Yes24()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "Yes24";
        market.ID = "1240";
        market.Url = "http://m.yes24.com/";
        market.Type = "문화";
        market.Icon = R.drawable.yes24;

        return  market;
    }

    private ShoppingMall Kyobo()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "교보문고";
        market.ID = "1250";
        market.Url = "http://mobile.kyobobook.co.kr/";
        market.Type = "문화";
        market.Icon = R.drawable.kyobo;

        return  market;
    }

    private ShoppingMall YoungPung()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "영푼문고";
        market.ID = "1260";
        market.Url = "https://www.ypbooks.co.kr";
        market.Type = "문화";
        market.Icon = R.drawable.ypbook;

        return  market;
    }

    private ShoppingMall RidiBooks()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "리디북스";
        market.ID = "1270";
        market.Url = "https://ridibooks.com";
        market.Type = "문화";
        market.Icon = R.drawable.ridibooks;

        return  market;
    }

    private ShoppingMall Bandi()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "반디앤루니스";
        market.ID = "1280";
        market.Url = "http://minibandi.com/";
        market.Type = "문화";
        market.Icon = R.drawable.bandi;

        return  market;
    }

    private ShoppingMall Ihreb()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "아이허브";
        market.ID = "1290";
        market.Url = "http://m.iherb.com/?l=ko";
        market.Type = "해외";
        market.Icon = R.drawable.iherb;

        return  market;
    }

    private ShoppingMall Amazon()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "아마존";
        market.ID = "1300";
        market.Url = "https://www.amazon.com/";
        market.Type = "해외";
        market.Icon = R.drawable.amazon;

        return  market;
    }

    private ShoppingMall Alibaba()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "알리바바";
        market.ID = "1310";
        market.Url = "https://m.alibaba.com";
        market.Type = "해외";
        market.Icon = R.drawable.alibaba;

        return  market;
    }

    private ShoppingMall Rakuten()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "라쿠텐";
        market.ID = "1320";
        market.Url = "http://global.rakuten.com/ko/";
        market.Type = "해외";
        market.Icon = R.drawable.rakuten;

        return  market;
    }

    private ShoppingMall Qoo10()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "Qoo10";
        market.ID = "1330";
        market.Url = "https://directjapan.qoo10.com/";
        market.Type = "해외";
        market.Icon = R.drawable.qoo10;

        return  market;
    }

    private ShoppingMall TenByTen()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "10x10";
        market.ID = "1340";
        market.Url = "http://m.10x10.co.kr/";
        market.Type = "생활";
        market.Icon = R.drawable.tenbyten;

        return  market;
    }

    private ShoppingMall Ikea()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "이케아";
        market.ID = "1350";
        market.Url = "http://m.ikea.com/";
        market.Type = "생활";
        market.Icon = R.drawable.ikea;

        return  market;
    }

    private ShoppingMall Hansam()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "한샘";
        market.ID = "1360";
        market.Url = "http://mall.hanssem.com/";
        market.Type = "생활";
        market.Icon = R.drawable.hansam;

        return  market;
    }

    private ShoppingMall ILRoom()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "일룸";
        market.ID = "1370";
        market.Url = "http://m.iloom.com/";
        market.Type = "생활";
        market.Icon = R.drawable.ilroom;

        return  market;
    }

    private ShoppingMall Daiso()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "다이소";
        market.ID = "1380";
        market.Url = "http://m.daisomall.co.kr/";
        market.Type = "생활";
        market.Icon = R.drawable.daiso;

        return  market;
    }

    private ShoppingMall HiMart()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "하이마트";
        market.ID = "1390";
        market.Url = "http://m.e-himart.co.kr/";
        market.Type = "디지털";
        market.Icon = R.drawable.himart;

        return  market;
    }

    private ShoppingMall Costco()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "코스트코";
        market.ID = "1400";
        market.Url = "https://www.costco.co.kr/";
        market.Type = "종합";
        market.Icon = R.drawable.costco;

        return  market;
    }

    private ShoppingMall Watsons()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "Watsons";
        market.ID = "1410";
        market.Url = "https://mshop.watsons.co.kr/";
        market.Type = "뷰티";
        market.Icon = R.drawable.watsons;

        return  market;
    }

    private ShoppingMall ABC()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "ABC마트";
        market.ID = "1420";
        market.Url = "https://www.abcmart.co.kr";
        market.Type = "패션";
        market.Icon = R.drawable.abc;

        return  market;
    }

    private ShoppingMall Lesmore()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "레스모아";
        market.ID = "1430";
        market.Url = "https://m.lesmore.com/";
        market.Type = "패션";
        market.Icon = R.drawable.lesmore;

        return  market;
    }

    private ShoppingMall Stylenanda()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "스타일난다";
        market.ID = "1440";
        market.Url = "http://m.stylenanda.com/";
        market.Type = "여성";
        market.Icon = R.drawable.stylenanda;

        return  market;
    }

    private ShoppingMall Dora()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "도라";
        market.ID = "1450";
        market.Url = "http://m.d-ora.com/";
        market.Type = "여성";
        market.Icon = R.drawable.dora;

        return  market;
    }

    private ShoppingMall Michyeora()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "미쳐라";
        market.ID = "1460";
        market.Url = "http://m.michyeora.com/";
        market.Type = "여성";
        market.Icon = R.drawable.michyeora;

        return  market;
    }

    private ShoppingMall SixSixGirls()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "육육걸즈";
        market.ID = "1470";
        market.Url = "http://m.66girls.co.kr/";
        market.Type = "여성";
        market.Icon = R.drawable.sixsix;

        return  market;
    }

    private ShoppingMall Bullang()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "불량소녀";
        market.ID = "1480";
        market.Url = "http://m.bullang.com/";
        market.Type = "여성";
        market.Icon = R.drawable.bullang;

        return  market;
    }

    private ShoppingMall Sonyunara()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "소녀나라";
        market.ID = "1490";
        market.Url = "http://m.sonyunara.com/";
        market.Type = "여성";
        market.Icon = R.drawable.sonyunara;

        return  market;
    }

    private ShoppingMall Teenbly()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "틴블리";
        market.ID = "1500";
        market.Url = "http://m.teenvely.com/";
        market.Type = "여성";
        market.Icon = R.drawable.teenbly;

        return  market;
    }

    private ShoppingMall Zara()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "ZARA";
        market.ID = "1510";
        market.Url = "http://m.zara.com/kr/";
        market.Type = "패션";
        market.Icon = R.drawable.zara;

        return  market;
    }

    private ShoppingMall HAndM()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "H&M";
        market.ID = "1520";
        market.Url = "http://m2.hm.com/m/ko_kr";
        market.Type = "패션";
        market.Icon = R.drawable.handm;

        return  market;
    }

    private ShoppingMall Mango()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "Mango";
        market.ID = "1530";
        market.Url = "http://shop.mango.com/KR";
        market.Type = "패션";
        market.Icon = R.drawable.mango;

        return  market;
    }

    private ShoppingMall Uniqlo()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "유니클로";
        market.ID = "1540";
        market.Url = "http://www.uniqlo.com/kr/ko/";
        market.Type = "패션";
        market.Icon = R.drawable.uniqlo;

        return  market;
    }

    private ShoppingMall Spao()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "스파오";
        market.ID = "1540";
        market.Url = "http://m-spao.elandmall.com/";
        market.Type = "패션";
        market.Icon = R.drawable.spao;

        return  market;
    }

    private ShoppingMall EightSconds()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "8Seconds";
        market.ID = "1550";
        market.Url = "http://m.ssfshop.com/main";
        market.Type = "패션";
        market.Icon = R.drawable.eightseconds;

        return  market;
    }

    private ShoppingMall Mutnam()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "멋남";
        market.ID = "1560";
        market.Url = "http://m.mutnam.com/";
        market.Type = "남성";
        market.Icon = R.drawable.mutnam;

        return  market;
    }

    private ShoppingMall Aboki()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "아보키";
        market.ID = "1570";
        market.Url = "http://www.aboki.net/m/";
        market.Type = "남성";
        market.Icon = R.drawable.aboki;

        return  market;
    }

    private ShoppingMall Tomonari()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "토모나리";
        market.ID = "1580";
        market.Url = "http://www.tomonari.co.kr/m";
        market.Type = "남성";
        market.Icon = R.drawable.tomonari;

        return  market;
    }

    private ShoppingMall JogunShop()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "조군샵";
        market.ID = "1590";
        market.Url = "http://www.jogunshop.com/m";
        market.Type = "남성";
        market.Icon = R.drawable.jogunshop;

        return  market;
    }

    private ShoppingMall SamsungOnlineStore()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "삼성온라인스토어";
        market.ID = "1600";
        market.Url = "http://store.samsung.com/";
        market.Type = "디지털";
        market.Icon = R.drawable.samsung;

        return  market;
    }

    private ShoppingMall NaverShopping()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "네이버쇼핑";
        market.ID = "1610";
        market.Url = "http://shopping2.naver.com/";
        market.Type = "비교";
        market.Icon = R.drawable.navershopping;

        return  market;
    }

    private ShoppingMall Danawa()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "다나와";
        market.ID = "1620";
        market.Url = "http://m.danawa.com/";
        market.Type = "비교";
        market.Icon = R.drawable.danawa;

        return  market;
    }

    private ShoppingMall WomanStalk()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "우먼스톡";
        market.ID = "1630";
        market.Url = "http://www.womanstalk.co.kr/";
        market.Type = "뷰티";
        market.Icon = R.drawable.womanstock;

        return  market;
    }

    private ShoppingMall MimiBox()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "미미박스";
        market.ID = "1640";
        market.Url = "http://m.memebox.com/";
        market.Type = "뷰티";
        market.Icon = R.drawable.mimibox;

        return  market;
    }

    private ShoppingMall Inisfree()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "이니스프리";
        market.ID = "1650";
        market.Url = "http://www.innisfree.co.kr/";
        market.Type = "뷰티";
        market.Icon = R.drawable.inisfree;

        return  market;
    }

    private ShoppingMall LFMall()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "LF몰";
        market.ID = "1660";
        market.Url = "http://m.lfmall.co.kr/";
        market.Type = "패션";
        market.Icon = R.drawable.lfmall;

        return  market;
    }

    private ShoppingMall Enuri()
    {
        ShoppingMall market = new ShoppingMall();
        market.Name = "에누리";
        market.ID = "1660";
        market.Url = "http://m.enuri.com/";
        market.Type = "비교";
        market.Icon = R.drawable.enuri;

        return  market;
    }
}