package com.chris.cartmoa.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chris.cartmoa.R;

/**
 * Created by phj on 2017-01-22.
 */

public class ShoppingMallListViewHolder extends RecyclerView.ViewHolder
{
    public TextView marketName;
    public LinearLayout rootLayout;
    public ImageView marketIcon;
    public ImageButton goShopping;

    public ShoppingMallListViewHolder(View itemView){
        super(itemView);

        marketName = (TextView)itemView.findViewById(R.id.marketName);
        rootLayout = (LinearLayout)itemView.findViewById(R.id.rootLayout);
        marketIcon = (ImageView)itemView.findViewById(R.id.marketIcon);
        goShopping =(ImageButton)itemView.findViewById(R.id.goShopping);
    }
}
