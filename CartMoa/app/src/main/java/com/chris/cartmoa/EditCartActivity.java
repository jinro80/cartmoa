package com.chris.cartmoa;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.chris.cartmoa.DB.DBHelper_Cart;
import com.chris.cartmoa.Models.CartModel;
import com.desmond.squarecamera.CameraActivity;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.process.BitmapProcessor;

import java.text.SimpleDateFormat;
import java.util.Date;

public class EditCartActivity extends AppCompatActivity {

    private static final int IMAGE_GALLERY_REQUEST = 1;
    private static final int IMAGE_CAMERA_REQUEST = 2;

    private LinearLayout emptyLayout;
    private ImageView prdouctImage;
    private EditText productName;
    private  EditText productPrice;
    private  EditText marketName;
    private  EditText productURL;
    private  EditText description;
    private Button completed;

    ImageLoader imageLoader;
    private DisplayImageOptions options;
    private ImageLoaderConfiguration config;


    DBHelper_Cart db;
    private  String imageURL = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_cart);

        db = new DBHelper_Cart(this);
        imageLoader = ImageLoader.getInstance(); // Get singleton instance
        config = ImageLoaderConfiguration.createDefault(this);

        options = new DisplayImageOptions.Builder()
                .imageScaleType(ImageScaleType.EXACTLY)
                .resetViewBeforeLoading(true)
                .showImageForEmptyUri(R.drawable.ic_warning_white_48dp)
                .showImageOnFail(R.drawable.ic_warning_white_48dp)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(false)
                .postProcessor(new BitmapProcessor() {
                    @Override
                    public Bitmap process(Bitmap bmp) {
                        return Bitmap.createScaledBitmap(bmp, prdouctImage.getWidth(), prdouctImage.getHeight(), false);
                    }
                })
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        imageLoader.init(config);


        emptyLayout = (LinearLayout)findViewById(R.id.emptyLayout);
        emptyLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showPicker();
            }
        });

        prdouctImage = (ImageView)findViewById(R.id.prdouctImage);
        prdouctImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showPicker();
            }
        });


        imageURL =MainActivity.selectedModel.ImageUrl;

        if(imageURL.equals(""))
        {
            emptyLayout.setVisibility(View.VISIBLE);
        }
        else
        {
            emptyLayout.setVisibility(View.GONE);
            imageLoader.displayImage(imageURL, prdouctImage);
        }


        productName = (EditText)findViewById(R.id.productName);
        productName.setText(MainActivity.selectedModel.Title);

        productPrice = (EditText)findViewById(R.id.productPrice);

         productPrice.setText(MainActivity.selectedModel.Price);


        marketName = (EditText)findViewById(R.id.marketName);
        marketName.setText(MainActivity.selectedModel.Market);


        productURL = (EditText)findViewById(R.id.productURL);
        productURL.setText(MainActivity.selectedModel.ProductUrl);

        description = (EditText)findViewById(R.id.description);
        description.setText(MainActivity.selectedModel.Description);

        completed = (Button)findViewById(R.id.completed);
        completed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(checkProductName()) {
                    DialogSimple();
                }
            }
        });

    }

    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void DialogSimple(){
        AlertDialog.Builder alt_bld = new AlertDialog.Builder(this);
        alt_bld.setMessage("확인").setCancelable(
                false).setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {


                        save();

                    }
                }).setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Action for 'NO' Button
                        dialog.cancel();
                    }
                });

        AlertDialog alert = alt_bld.create();
        // Title for AlertDialog

        alert.setTitle("이대로 완료 하시겠습니까?");



        alert.show();
    }

    private  boolean checkProductName()
    {
        if(productName.getText().length() < 1)
        {
            Toast.makeText(this,
                    "상품명을 입력하세요.", Toast.LENGTH_LONG).show();
            return false;
        }

        return  true;
    }

    private void save()
    {

        try {

            CartModel model = new CartModel();
            model.ID = MainActivity.selectedModel.ID;
            model.Title = productName.getText().toString();
            model.Description = description.getText().toString();
            model.ImageUrl = imageURL;
            model.Market = marketName.getText().toString();
            model.Price = productPrice.getText().toString();
            model.ProductUrl = productURL.getText().toString();
            model.RegDate =MainActivity.selectedModel.RegDate;

            /*SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

            model.RegDate = simpleDateFormat.format(new Date());*/
            db.UpdateCart(model);


            MainActivity.selectedModel = model;
            Toast.makeText(this,
                    "장바구니 수정 성공", Toast.LENGTH_LONG).show();

            setResult(RESULT_OK);
            finish();
        }
        catch (Exception e)
        {
            setResult(RESULT_CANCELED);
            Toast.makeText(this,
                    "오류, 장바구니 담기가 실패했습니다.", Toast.LENGTH_LONG).show();
            return;
        }
    }

    private  void showPicker() {
        final CharSequence[] items = new CharSequence[]{"카메라", "갤러리"};
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("상품 이미지 추가하기");
        dialog.setItems(items, new DialogInterface.OnClickListener() {
            // 리스트 선택 시 이벤트
            public void onClick(DialogInterface dialog, int which) {
                if(which == 0)
                {
                    photoCameraIntent();
                }
                else
                {
                    photoGalleryIntent();
                }
            }
        });
        dialog.show();
    }




    private void photoCameraIntent(){

        Intent startCustomCameraIntent = new Intent(this, CameraActivity.class);
        startActivityForResult(startCustomCameraIntent, IMAGE_CAMERA_REQUEST);

    }


    private void photoGalleryIntent(){

        Intent intent;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
        }else{
            intent = new Intent(Intent.ACTION_GET_CONTENT);
        }

        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_picture_title)), IMAGE_GALLERY_REQUEST);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == IMAGE_GALLERY_REQUEST){
            if (resultCode == RESULT_OK){
                Uri selectedImageUri = data.getData();
                if (selectedImageUri != null){

                    imageURL = selectedImageUri.toString();
                    imageLoader.displayImage(imageURL, prdouctImage);
                    emptyLayout.setVisibility(View.GONE);
                }else{

                    imageURL = "";
                    emptyLayout.setVisibility(View.VISIBLE);
                }
            }
        }

        else if (requestCode == IMAGE_CAMERA_REQUEST){
            if (resultCode == RESULT_OK) {

                Uri photoUri = data.getData();

                if (photoUri != null) {
                    imageURL = photoUri.toString();
                    imageLoader.displayImage(imageURL.toString(), prdouctImage);
                    emptyLayout.setVisibility(View.GONE);
                } else {
                    imageURL = "";
                    emptyLayout.setVisibility(View.VISIBLE);
                }


            }
        }
    }

}
