package com.chris.cartmoa.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.chris.cartmoa.Models.CartModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by phj on 2017-01-19.
 */

public class DBHelper_Cart extends SQLiteOpenHelper
{
    private Context context;

    public DBHelper_Cart(Context context) {
        super(context, "Cart", null, 1);

        this.context = context;
    }

    public boolean testDB() {
        SQLiteDatabase db = getReadableDatabase();

        return  db.isOpen();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        StringBuffer sb = new StringBuffer();
        sb.append(" CREATE TABLE Cart ( " );
        sb.append(" _ID INTEGER PRIMARY KEY AUTOINCREMENT, " );
        sb.append(" Title TEXT, " );
        sb.append(" Description TEXT, " );
        sb.append(" RegDate TEXT,  " );
        sb.append(" ProductUrl TEXT,  " );
        sb.append(" Market TEXT,  " );
        sb.append(" Price TEXT,  " );
        sb.append(" ImageUrl TEXT ); " );

        db.execSQL(sb.toString());

        //Toast.makeText(context, "Table 생성됨" , Toast.LENGTH_SHORT).show();
    }

    /**
     * Application 의 버전이 올라가 Table 구조가 변경 되었을 때, 실행된다.
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        if ( oldVersion == 1 && newVersion == 2 ){
            StringBuffer sb = new StringBuffer();
            //sb.append(" ALTER TABLE DeliveryHistory ADD ADDRESS TEXT ");
            db.execSQL(sb.toString());

            //Toast.makeText(context, "Version 올라간다." , Toast.LENGTH_SHORT).show();
        }
//        else if ( oldVersion == 1 && newVersion == 3 ) {
//
//        }
    }


    public void UpdateCart(CartModel info) {
        // 1. 쓰기 가능한 DB 객체를 가져온다.
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("Title", info.Title);
        values.put("Description", info.Description);
        values.put("RegDate", info.RegDate);
        values.put("ImageUrl", info.ImageUrl);

        values.put("ProductUrl", info.ProductUrl);
        values.put("Market", info.Market);
        values.put("Price", info.Price);


        db.update("Cart", values, "_ID=?", new String[]{info.ID} );
        db.close();
    }

    public void AddCart(CartModel info) {
        // 1. 쓰기 가능한 DB 객체를 가져온다.
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("Title", info.Title);
        values.put("Description", info.Description);
        values.put("RegDate", info.RegDate);
        values.put("ImageUrl", info.ImageUrl);
        values.put("ProductUrl", info.ProductUrl);
        values.put("Market", info.Market);
        values.put("Price", info.Price);

        db.insert("Cart", null, values);

        db.close();

    }

    public List<CartModel> GetAllCart(){

        StringBuffer sb = new StringBuffer();
        sb.append(" SELECT _ID, Title, Description, RegDate, ImageUrl, ProductUrl, Market, Price FROM Cart order by _ID desc ");

        // 읽기 전용 DB 객체를 가져온다.
        SQLiteDatabase db = getReadableDatabase();

        // Select 해 온다.
        Cursor cursor = db.rawQuery(sb.toString(), null);

        List<CartModel> cartList = new ArrayList<CartModel>();

        Log.d("RESULT", cursor.getCount() + "");

        CartModel cart = null;
        while ( cursor.moveToNext() ) {
            cart = new CartModel();
            cart.ID = String.valueOf(cursor.getInt(0));
            cart.Title = cursor.getString(1);
            cart.Description = cursor.getString(2);
            cart.RegDate = cursor.getString(3);
            cart.ImageUrl = cursor.getString(4);

            cart.ProductUrl = cursor.getString(5);
            cart.Market = cursor.getString(6);
            cart.Price = cursor.getString(7);

            cartList.add(cart);
        }

        db.close();
        return cartList;

    }

    public List<CartModel> GetCart(String keyword){

        StringBuffer sb = new StringBuffer();
        sb.append(" SELECT _ID, Title, Description, RegDate, ImageUrl, ProductUrl, Market, Price FROM Cart where title LIKE '%" + keyword + "%' or Description LIKE '%" +keyword  +"%' order by _ID desc ");

        // 읽기 전용 DB 객체를 가져온다.
        SQLiteDatabase db = getReadableDatabase();

        // Select 해 온다.
        Cursor cursor = db.rawQuery(sb.toString(), null);

        List<CartModel> cartList = new ArrayList<CartModel>();

        Log.d("RESULT", cursor.getCount() + "");

        CartModel cart = null;
        while ( cursor.moveToNext() ) {
            cart = new CartModel();
            cart.ID = String.valueOf(cursor.getInt(0));
            cart.Title = cursor.getString(1);
            cart.Description = cursor.getString(2);
            cart.RegDate = cursor.getString(3);
            cart.ImageUrl = cursor.getString(4);
            cart.ProductUrl = cursor.getString(5);
            cart.Market = cursor.getString(6);
            cart.Price = cursor.getString(7);
            cartList.add(cart);
        }

        db.close();
        return cartList;

    }

    public List<CartModel> GetAllCart(String sort){
//sort == asc or desc

        StringBuffer sb = new StringBuffer();
        sb.append(" SELECT _ID, Title, Description, RegDate, ImageUrl, ProductUrl, Market, Price FROM Cart order by _ID  " + sort);

        // 읽기 전용 DB 객체를 가져온다.
        SQLiteDatabase db = getReadableDatabase();

        // Select 해 온다.
        Cursor cursor = db.rawQuery(sb.toString(), null);

        List<CartModel> cartList = new ArrayList<CartModel>();

        Log.d("RESULT", cursor.getCount() + "");

        CartModel cart = null;
        while ( cursor.moveToNext() ) {
            cart = new CartModel();
            cart = new CartModel();
            cart.ID = String.valueOf(cursor.getInt(0));
            cart.Title = cursor.getString(1);
            cart.Description = cursor.getString(2);
            cart.RegDate = cursor.getString(3);
            cart.ImageUrl = cursor.getString(4);
            cart.ProductUrl = cursor.getString(5);
            cart.Market = cursor.getString(6);
            cart.Price = cursor.getString(7);
            cartList.add(cart);
        }

        db.close();
        return cartList;

    }



    public boolean  DeleteCart(String id)
    {
        SQLiteDatabase db = getWritableDatabase();
        try {

            db.delete("Cart", "_ID=?", new String[]{id});

            Log.i("db", id + "정상적으로 삭제되었습니다.");
        }
        catch (Exception e)
        {
            Log.i("db", e.toString());
            return  false;
        }
        finally {
            db.close();
        }

        return  true;
    }
}