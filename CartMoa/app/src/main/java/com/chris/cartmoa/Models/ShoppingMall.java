package com.chris.cartmoa.Models;

/**
 * Created by phj on 2017-01-22.
 */

public class ShoppingMall {

    //종합쇼핑몰, 여성, 남성, 뷰티, 식품, 잡화, 생활용품, 해외, 직구, 전자, 소셜마켓,
    public String ID = "";
    public String Name = "";
    public String Type = "";
    public String Url = "";
    public int Icon = -1;
}
