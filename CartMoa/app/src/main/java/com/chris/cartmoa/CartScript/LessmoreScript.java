package com.chris.cartmoa.CartScript;

import com.chris.cartmoa.Models.CartModel;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by phj on 2017-01-24.
 */

public class LessmoreScript implements IWebSrcipt
{
    public CartModel GetCartModel(String response)
    {
        CartModel model = new CartModel();
        model.Market = "레스모아";

        //pname:\s+'(?<image>[^']+)
        Pattern pattern = Pattern.compile("class=\"breadcrumb\">([^<]+)");

        Matcher matcher = pattern.matcher(response);

        if(matcher.groupCount() > 0) {
            while (matcher.find()) {
                model.Title = matcher.group(1).toString().trim();
                break;
            }
        }

        pattern = Pattern.compile("<li><img\\s+src=\"([^\"]+)");
        matcher = pattern.matcher(response);

        if(matcher.groupCount() > 0) {
            while (matcher.find()) {
                model.ImageUrl = "https://m.lesmore.com"+ matcher.group(1).toString();
                break;
            }
        }

        pattern = Pattern.compile("최적가격\\s+.*?/a><span>([^<]+)");
        matcher = pattern.matcher(response);

        if(matcher.groupCount() > 0) {
            while (matcher.find()) {
                model.Price = matcher.group(1).toString();
                break;
            }
        }

        return  model;
    }

    public  boolean CanDoCart(String url)
    {

        if(url.toLowerCase().contains("productcode="))
        {
            return  true;
        }

        return  false;
    }
}
