package com.chris.cartmoa.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.chris.cartmoa.Models.DeliveryHistory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by phj on 2017-01-22.
 */

public class DBHelper_Delivery extends SQLiteOpenHelper
{
    private Context context;

    public DBHelper_Delivery(Context context) {
        super(context, "DeliveryHistory", null, 1);

        this.context = context;
    }

    public boolean testDB() {
        SQLiteDatabase db = getReadableDatabase();

        return  db.isOpen();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        StringBuffer sb = new StringBuffer();
        sb.append(" CREATE TABLE DeliveryHistory ( " );
        sb.append(" _ID INTEGER PRIMARY KEY AUTOINCREMENT, " );
        sb.append(" InvoceNumber TEXT, " );
        sb.append(" Comment TEXT, " );
        sb.append(" Complete TEXT,  " );
        sb.append(" RegDate TEXT,  " );
        sb.append(" Company TEXT, " );
        sb.append(" Level TEXT, " );
        sb.append(" CompanyCode TEXT ); " );

        db.execSQL(sb.toString());

        //Toast.makeText(context, "Table 생성됨" , Toast.LENGTH_SHORT).show();
    }

    /**
     * Application 의 버전이 올라가 Table 구조가 변경 되었을 때, 실행된다.
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        if ( oldVersion == 1 && newVersion == 2 ){
            StringBuffer sb = new StringBuffer();
            //sb.append(" ALTER TABLE DeliveryHistory ADD ADDRESS TEXT ");
            db.execSQL(sb.toString());

            //Toast.makeText(context, "Version 올라간다." , Toast.LENGTH_SHORT).show();
        }
//        else if ( oldVersion == 1 && newVersion == 3 ) {
//
//        }
    }

    public void AddDeliveryHistory(DeliveryHistory info) {
        // 1. 쓰기 가능한 DB 객체를 가져온다.
        SQLiteDatabase db = getWritableDatabase();




        ContentValues values = new ContentValues();
        values.put("InvoceNumber", info.InvoceNumber); // Shop Name
        values.put("Comment", info.Comment); // Shop Phone Number
        values.put("Complete", info.Complete); // Shop Name
        values.put("RegDate", info.RegDate); // Shop Phone Number
        values.put("Company", info.Company); // Shop Name
        values.put("Level", info.Level); // Shop Name
        values.put("CompanyCode", info.CompanyCode); // Shop Phone Number
        db.insert("DeliveryHistory", null, values);
        // 2. Person Data 를 Insert 한다.
    /*    StringBuffer sb = new StringBuffer();
        sb.append(" INSERT INTO DeliveryHistory ( ");
        sb.append(" InvoceNumber, Comment, Complete, RegDate, Company, CompanyCode )");
        sb.append(" VALUES ( ?, ?, ?, ?, ?, ? )" );*/
//        sb.append(" VALUES ( #NAME#, #AGE#, #PHONE# ) ");
//
//        String query = sb.toString();
//        query = query.replace("#NAME#", "'" + person.getName() + "'");
//        query = query.replace("#AGE#", person.getAge() );
//        query = query.replace("#PHONE#", "'" + person.getPhone() + "'");
//
//        db.execSQL(query);
        //db.execSQL( sb.toString(), new Object[]{ info.Comment, info.Complete, info.RegDate, info.RegDate, info.Company, info.CompanyCode } );
        db.close();
        //Toast.makeText(context, "등록완료", Toast.LENGTH_SHORT).show();
    }

    public List<DeliveryHistory> GetAllDeliveryHistory(){

        StringBuffer sb = new StringBuffer();
        sb.append(" SELECT InvoceNumber, Comment, Complete, RegDate, Company, CompanyCode, Level FROM DeliveryHistory order by _ID desc ");

        // 읽기 전용 DB 객체를 가져온다.
        SQLiteDatabase db = getReadableDatabase();

        // Select 해 온다.
        Cursor cursor = db.rawQuery(sb.toString(), null);

        List<DeliveryHistory> historyList = new ArrayList<DeliveryHistory>();

        Log.d("RESULT", cursor.getCount() + "");

        DeliveryHistory history = null;
        while ( cursor.moveToNext() ) {
            history = new DeliveryHistory();
            history.InvoceNumber = cursor.getString(0);
            history.Comment = cursor.getString(1);
            history.Complete = cursor.getString(2);
            history.RegDate = cursor.getString(3);
            history.Company = cursor.getString(4);
            history.CompanyCode = cursor.getString(5);
            history.Level = cursor.getString(6);
            historyList.add(history);
        }

        db.close();
        return historyList;

    }

    public boolean  DeleteDeliveryInfo(String invoceNumber)
    {
        SQLiteDatabase db = getWritableDatabase();
        try {

            db.delete("DeliveryHistory", "InvoceNumber=?", new String[]{invoceNumber});

            Log.i("db", invoceNumber + "정상적으로 삭제되었습니다.");
        }
        catch (Exception e)
        {
            Log.i("db", e.toString());
            return  false;
        }
        finally {
            db.close();
        }

        return  true;
    }
}
