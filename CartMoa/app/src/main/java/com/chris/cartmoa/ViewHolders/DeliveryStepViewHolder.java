package com.chris.cartmoa.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.chris.cartmoa.R;

/**
 * Created by phj on 2017-01-22.
 */

public class DeliveryStepViewHolder extends RecyclerView.ViewHolder
{
    public TextView time;
    public TextView where;
    public TextView state;

    public DeliveryStepViewHolder(View itemView){
        super(itemView);

        time = (TextView)itemView.findViewById(R.id.time);
        where = (TextView)itemView.findViewById(R.id.where);
        state = (TextView)itemView.findViewById(R.id.state);
    }
}
