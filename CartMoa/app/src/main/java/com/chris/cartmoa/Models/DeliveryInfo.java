package com.chris.cartmoa.Models;

import java.util.Date;
import java.util.List;


/**
 * Created by phj on 2017-01-22.
 */

public class DeliveryInfo {

    public boolean complete;
    public String invoiceNo;
    public String itemName;
    public String receiverAddr;
    public String receiverName;
    public String recipient;
    public String senderName;
    public String DeliveryCompany;
    public Date RegDate;
    public  String level = "1";

    public List<TrackingDetail> trackingDetails;


}
