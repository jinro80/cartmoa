package com.chris.cartmoa.CartScript;

import com.chris.cartmoa.Models.CartModel;

/**
 * Created by phj on 2017-01-24.
 */

public interface IWebSrcipt {

    public CartModel GetCartModel(String response);
    public  boolean CanDoCart(String url);

}
