package com.chris.cartmoa.Models;

/**
 * Created by phj on 2017-01-19.
 */

public class CartModel {

    public String  ID = "";
    public String  Title = "";
    public String  Description = "";
    public String  RegDate = "0000-00-00";
    public String  ImageUrl = "";
    public String  ProductUrl = "";
    public String  Market = "상점";
    public String  Price ="0";
}
