package com.chris.cartmoa.CartScript;

import com.chris.cartmoa.Models.CartModel;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by phj on 2017-01-24.
 */

public class LotteComScript implements IWebSrcipt
{
    public CartModel GetCartModel(String response)
    {
        CartModel model = new CartModel();
        model.Market = "롯데닷컴";

        //pname:\s+'(?<image>[^']+)
        Pattern pattern = Pattern.compile("property=\"og:title\"\\s+content=\"([^\"]+)");

        Matcher matcher = pattern.matcher(response);

        if(matcher.groupCount() > 0) {
            while (matcher.find()) {
                model.Title = matcher.group(1).toString();
                break;
            }
        }

        pattern = Pattern.compile("property=\"og:image\"\\s+content=\"([^\"]+)");
        matcher = pattern.matcher(response);

        if(matcher.groupCount() > 0) {
            while (matcher.find()) {
                model.ImageUrl = matcher.group(1).toString();
                break;
            }
        }

        pattern = Pattern.compile("var\\s+salePrc\\s+=\\s+'([^']+)");
        matcher = pattern.matcher(response);

        if(matcher.groupCount() > 0) {
            while (matcher.find()) {
                model.Price = matcher.group(1).toString();
                break;
            }
        }

        return  model;
    }

    public  boolean CanDoCart(String url)
    {

      /*  if(url.toLowerCase().contains("goods_no="))
        {
            return  true;
        }
*/
        return  false;
    }
}
