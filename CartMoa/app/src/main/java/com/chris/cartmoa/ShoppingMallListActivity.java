package com.chris.cartmoa;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;

import com.chris.cartmoa.Adapters.ShoppingMallListAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class ShoppingMallListActivity extends AppCompatActivity {

    RecyclerView recycler_view;
    ShoppingMallListAdapter adapter;
    ImageButton backButton;
    ImageButton filterButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_mall_list);

        AdView mAdView = (AdView)findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("CFD62DC52635837D93B1BB6949AA1A6A").build();
        mAdView.loadAd(adRequest);


        recycler_view = (RecyclerView)findViewById(R.id.recycler_view);
        adapter = new ShoppingMallListAdapter(this);

        recycler_view.setAdapter(adapter);
        recycler_view.setLayoutManager(new LinearLayoutManager(this));

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }

        backButton = (ImageButton)findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                  finish();
            }
        });


        filterButton = (ImageButton) findViewById(R.id.filterButton);
        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popup = new PopupMenu(ShoppingMallListActivity.this, filterButton);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.filter_menu, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {

                            case R.id.popup_all :

                                //전체
                                adapter.SetAllShoppingMall();

                                break;
                            case R.id.popup_total :

                                //종합
                                adapter.SetTotal();

                                break;

                            case R.id.popup_fassion :

                                //패션
                                adapter.SetFassion();

                                break;


                            case R.id.popup_female :

                                adapter.SetFemale();

                                break;

                            case R.id.popup_male :

                                //남성
                                adapter.SetMale();

                                break;

                            case R.id.popup_beauty :

                                adapter.SetBeauty();

                                break;

                            case R.id.popup_digital :

                                adapter.SetDigital();

                                break;

                            case R.id.popup_home :

                                adapter.SetHome();

                                break;

                            case R.id.popup_oversea :

                                adapter.SetOverSea();

                                break;

                            case R.id.popup_compare :

                                adapter.SetCompare();

                                break;

                            case R.id.popup_homesopping :

                                adapter.SetHomshopping();

                                break;

                            case R.id.popup_culture :


                                adapter.SetCulture();

                                break;
                        }

                        return true;
                    }
                });

                popup.show();//showing popup menu
            }
        });

    }

}
