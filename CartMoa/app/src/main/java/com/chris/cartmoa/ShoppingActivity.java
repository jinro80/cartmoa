package com.chris.cartmoa;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chris.cartmoa.CartScript.IWebSrcipt;
import com.chris.cartmoa.CartScript.ScriptUtil;
import com.chris.cartmoa.DB.DBHelper_Cart;
import com.chris.cartmoa.Models.CartModel;
import com.desmond.squarecamera.CameraActivity;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.process.BitmapProcessor;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import im.delight.android.webview.AdvancedWebView;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ShoppingActivity extends AppCompatActivity implements AdvancedWebView.Listener {

    private static final int IMAGE_GALLERY_REQUEST = 1;
    private static final int IMAGE_CAMERA_REQUEST = 2;
    static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    static final JsonFactory JSON_FACTORY = new JacksonFactory();

    ImageLoader imageLoader;
    private DisplayImageOptions options;
    private ImageLoaderConfiguration config;

    String productUrl ="";

    DBHelper_Cart db;
    private  String imageURL = "";

    private AdvancedWebView mWebView;
    private ProgressDialog mProgressDialog;
    ImageButton backButton;
    ImageButton writeCartButton;
    ImageButton shoppingCartButton;

    LinearLayout layoutWriteCart;
    ImageView productImage;
    EditText productTitle;
    ImageButton closeForm;
    EditText productDescription;
    EditText productPrice;
    Button cartCompleted;
    IWebSrcipt webSrcipt;
    CartModel model;
    boolean fromCartButton = true;
    String marketName ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping);


        Intent intent = getIntent();
        String url = intent.getStringExtra("url");
        marketName = intent.getStringExtra("market");

        if(webSrcipt == null) {
            webSrcipt = ScriptUtil.ScriptFactory(marketName);
        }

        db = new DBHelper_Cart(this);
        imageLoader = ImageLoader.getInstance(); // Get singleton instance
        config = ImageLoaderConfiguration.createDefault(this);

        options = new DisplayImageOptions.Builder()
                .imageScaleType(ImageScaleType.EXACTLY)
                .resetViewBeforeLoading(true)
                .showImageForEmptyUri(R.drawable.ic_warning_white_48dp)
                .showImageOnFail(R.drawable.ic_warning_white_48dp)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(false)
                .postProcessor(new BitmapProcessor() {
                    @Override
                    public Bitmap process(Bitmap bmp) {
                        return Bitmap.createScaledBitmap(bmp, productImage.getWidth(), productImage.getHeight(), false);
                    }
                })
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        imageLoader.init(config);

        TextView activityLabel = (TextView)findViewById(R.id.activityLabel);
        activityLabel.setText(marketName);

        //장바구니 직접 입력
        layoutWriteCart = (LinearLayout) findViewById(R.id.layoutWriteCart);
        productTitle = (EditText)findViewById(R.id.productTitle);
        productDescription = (EditText)findViewById(R.id.productDescription);
        productPrice = (EditText)findViewById(R.id.productPrice);
        productImage = (ImageView)findViewById(R.id.prouctImage);
        productImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //장바구니 직접 입력 상품사진.
                showPicker();
            }
        });

        closeForm = (ImageButton)findViewById(R.id.closeForm);
        closeForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                model = null;
                productPrice.setText("");
                productTitle.setText("");
                productDescription.setText("");
                productImage.setImageResource(R.drawable.ic_add_a_photo_white_48dp);
                layoutWriteCart.setVisibility(View.GONE);
            }
        });

        cartCompleted = (Button)findViewById(R.id.cartCompleted);
        cartCompleted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(checkProductName()) {
                    DialogSimple();
                }

            }
        });

        backButton = (ImageButton)findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DialogFinish();
            }
        });

        writeCartButton = (ImageButton)findViewById(R.id.writeCartButton);
        writeCartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fromCartButton= false;

                    productPrice.setText("");
                    productTitle.setText("");
                    productDescription.setText("");
                    productImage.setImageResource(R.drawable.ic_add_a_photo_white_48dp);
                    layoutWriteCart.setVisibility(View.VISIBLE);

            }
        });


        shoppingCartButton = (ImageButton)findViewById(R.id.shoppingCartButton);
        shoppingCartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fromCartButton = true;
                GetHttpResponse();
            }
        });


        mWebView = (AdvancedWebView) findViewById(R.id.webview);
        mWebView.setListener(this, this);
        mWebView.loadUrl(url);
    }


    private void DialogFinish()
    {
        AlertDialog.Builder alt_bld = new AlertDialog.Builder(this);
        alt_bld.setMessage("쇼핑을 그만 하시겠습니까?").setCancelable(
                false).setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        finish();

                    }
                }).setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Action for 'NO' Button
                        dialog.cancel();
                        return;
                    }
                });

        AlertDialog alert = alt_bld.create();
        // Title for AlertDialog

        alert.setTitle("확인");



        alert.show();
    }

    private void GetHttpResponse()
    {
        if(productUrl.length() < 1) return;

        mProgressDialog = ProgressDialog.show(this, "", "Loading...", true);

        new Thread() {
            public void run() {

                try {
                    String response = "";



                    response = HttpGet(productUrl);


                    model = webSrcipt.GetCartModel(response);
                    handler.sendEmptyMessage(0);

                } catch (Exception e) {

                    handler.sendEmptyMessage(1);
                }
            }
        }.start();
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 0) {   // Message id 가 0 이면

                if (mProgressDialog!=null) {
                    mProgressDialog.dismiss();
                }

                if(model != null) {

                        if(marketName.equals("알리바바") || marketName.equals("아마존") || marketName.equals("아이허브") || marketName.equals("라쿠텐") || marketName.equals("Qoo10")) {


                            productPrice.setText( "$" + model.Price);
                        }
                    else {
                            productPrice.setText(model.Price + "원");
                        }
                        productTitle.setText(model.Title);
                        productDescription.setText("");
                        imageLoader.displayImage(model.ImageUrl, productImage);
                        layoutWriteCart.setVisibility(View.VISIBLE);

                }
            }
            else
            {
                //에러...

                ShowAlret("알림", "장바구니 간편 담기에 실패했습니다. 직접 입력해서 장바구니를 담아보세요.");

                if (mProgressDialog!=null) {
                    mProgressDialog.dismiss();
                }

                productPrice.setText("");
                productTitle.setText("");
                productDescription.setText("");
                productImage.setImageResource(R.drawable.ic_add_a_photo_white_48dp);
            }

        }
    };

    private void ShowToast(String msg)
    {
        Toast.makeText(this,
                msg , Toast.LENGTH_LONG).show();

    }


    private  void ShowAlret(String title, String message)
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setPositiveButton("확인", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alert.setTitle(title);
        alert.setMessage(message);
        alert.show();
    }


    private String HttpGet(String url) throws IOException {

        OkHttpClient client = new OkHttpClient();


        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();

        String header = response.headers().toString();
        return response.body().string();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mWebView.onResume();

    }

    @Override
    protected void onPause() {
        mWebView.onPause();

        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mWebView.onDestroy();

        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        mWebView.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == IMAGE_GALLERY_REQUEST){
            if (resultCode == RESULT_OK){
                Uri selectedImageUri = intent.getData();
                if (selectedImageUri != null){

                    imageURL = selectedImageUri.toString();
                    imageLoader.displayImage(imageURL, productImage);

                }else{

                    imageURL = "";
                }
            }
        }

        else if (requestCode == IMAGE_CAMERA_REQUEST){
            if (resultCode == RESULT_OK) {

                Uri photoUri = intent.getData();

                if (photoUri != null) {
                    imageURL = photoUri.toString();
                    imageLoader.displayImage(imageURL.toString(), productImage);

                } else {
                    imageURL = "";

                }


            }
        }



    }

    @Override
    public void onBackPressed() {

        if(layoutWriteCart.getVisibility() == View.VISIBLE)
        {
            layoutWriteCart.setVisibility(View.GONE);
            return;
        }

        if (!mWebView.onBackPressed()) { return; }


        AlertDialog.Builder alt_bld = new AlertDialog.Builder(this);
        alt_bld.setMessage("쇼핑을 그만 하시겠습니까?").setCancelable(
                false).setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        finish();

                    }
                }).setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Action for 'NO' Button
                        dialog.cancel();
                        return;
                    }
                });

        AlertDialog alert = alt_bld.create();
        // Title for AlertDialog

        alert.setTitle("확인");



        alert.show();


        //super.onBackPressed();
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {

        if(webSrcipt == null) {
            webSrcipt = ScriptUtil.ScriptFactory(marketName);
        }


        if(mProgressDialog== null) {

            mProgressDialog = ProgressDialog.show(this, "", "Loading...", true);
            return;
        }

        if(!mProgressDialog.isShowing())
            mProgressDialog = ProgressDialog.show(this, "", "Loading...", true);
    }

    @Override
    public void onPageFinished(String url)
    {
        if(webSrcipt == null) {
            webSrcipt = ScriptUtil.ScriptFactory(marketName);
        }

        if (mProgressDialog!=null){
            mProgressDialog.dismiss();
        }

        if(webSrcipt != null) {
            if (webSrcipt.CanDoCart(url)) {
                shoppingCartButton.setVisibility(View.VISIBLE);
                productUrl = url;
            } else {
                shoppingCartButton.setVisibility(View.GONE);
                productUrl = "";
            }
        }
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl)
    {
        if (mProgressDialog!=null){
            mProgressDialog.dismiss();

            shoppingCartButton.setVisibility(View.GONE);
            productUrl ="";
        }
    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent)
    {
        if (mProgressDialog!=null){
            mProgressDialog.dismiss();

            shoppingCartButton.setVisibility(View.GONE);
            productUrl ="";
        }
    }

    @Override
    public void onExternalPageRequest(String url) { }


    private void DialogSimple(){
        AlertDialog.Builder alt_bld = new AlertDialog.Builder(this);
        alt_bld.setMessage("이대로 완료 하시겠습니까?").setCancelable(
                false).setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {


                        save();

                    }
                }).setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Action for 'NO' Button
                        dialog.cancel();
                    }
                });

        AlertDialog alert = alt_bld.create();
        // Title for AlertDialog

        alert.setTitle("확인");



        alert.show();
    }

    private  boolean checkProductName()
    {
        if(productTitle.getText().length() < 1)
        {
            Toast.makeText(this,
                    "상품명을 입력하세요.", Toast.LENGTH_LONG).show();
            return false;
        }

        return  true;
    }

    private void save()
    {

        try {

            if (!fromCartButton) {

                if (model == null) {
                    model = new CartModel();
                }

                model.ProductUrl = productUrl;
                model.Title = productTitle.getText().toString();
                model.Price = productPrice.getText().toString();
                model.ImageUrl = imageURL;
                model.Market = marketName;
            }

            model.ProductUrl = productUrl;
            model.Market = marketName;
            model.Description = productDescription.getText().toString();


            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

            model.RegDate = simpleDateFormat.format(new Date());
            db.AddCart(model);


            Toast.makeText(this,
                    "장바구니 담기 성공", Toast.LENGTH_LONG).show();

            setResult(RESULT_OK);

            model = null;
            productPrice.setText("");
            productTitle.setText("");
            productDescription.setText("");
            productImage.setImageResource(R.drawable.ic_add_a_photo_white_48dp);
            layoutWriteCart.setVisibility(View.GONE);
        }
        catch (Exception e)
        {
            model = null;
            productPrice.setText("");
            productTitle.setText("");
            productDescription.setText("");
            productImage.setImageResource(R.drawable.ic_add_a_photo_white_48dp);
            layoutWriteCart.setVisibility(View.GONE);

            setResult(RESULT_CANCELED);
            Toast.makeText(this,
                    "오류, 장바구니 담기가 실패했습니다.", Toast.LENGTH_LONG).show();
            return;
        }
    }

    private  void showPicker() {
        final CharSequence[] items = new CharSequence[]{"카메라", "갤러리"};
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("상품 이미지 추가하기");
        dialog.setItems(items, new DialogInterface.OnClickListener() {
            // 리스트 선택 시 이벤트
            public void onClick(DialogInterface dialog, int which) {
                if(which == 0)
                {
                    photoCameraIntent();
                }
                else
                {
                    photoGalleryIntent();
                }
            }
        });
        dialog.show();
    }




    private void photoCameraIntent(){

        Intent startCustomCameraIntent = new Intent(this, CameraActivity.class);
        startActivityForResult(startCustomCameraIntent, IMAGE_CAMERA_REQUEST);

    }


    private void photoGalleryIntent(){

        Intent intent;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
        }else{
            intent = new Intent(Intent.ACTION_GET_CONTENT);
        }

        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_picture_title)), IMAGE_GALLERY_REQUEST);

    }
}
