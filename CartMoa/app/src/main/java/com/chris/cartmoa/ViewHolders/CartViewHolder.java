package com.chris.cartmoa.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.chris.cartmoa.R;
/**
 * Created by phj on 2017-01-19.
 */

public class CartViewHolder extends RecyclerView.ViewHolder
{
    public TextView registDate;
    public TextView productName;
    public TextView description;
    public LinearLayout rootLayout;
    public ImageView prouctImage;
    public ImageButton deleteCart;
    public TextView martName;
    public TextView productPrice;

    public CartViewHolder(View itemView){
        super(itemView);

        registDate = (TextView)itemView.findViewById(R.id.registDate);
        productName = (TextView)itemView.findViewById(R.id.productName);
        description = (TextView)itemView.findViewById(R.id.description);
        rootLayout = (LinearLayout)itemView.findViewById(R.id.rootLayout);
        prouctImage = (ImageView)itemView.findViewById(R.id.prouctImage);
        deleteCart =(ImageButton)itemView.findViewById(R.id.deleteCart);
        martName = (TextView) itemView.findViewById(R.id.martName);
        productPrice = (TextView)itemView.findViewById(R.id.productPrice);
    }
}
