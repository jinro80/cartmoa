package com.chris.cartmoa;

import android.*;
import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.chris.cartmoa.Adapters.CartAdapter;
import com.chris.cartmoa.Adapters.SpinnerAdapter;
import com.chris.cartmoa.Models.CartModel;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static CartModel selectedModel = null;
    RecyclerView recycler_view;
    Spinner mSpinner;
    CartAdapter adapter;
    public SwipeRefreshLayout mSwipeRefresh;
    ImageButton shoppingButton;
    ImageButton deliveryButton;
    ImageButton menuButton;
    String sorting = "desc";

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 0:

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                    ActivityCompat.requestPermissions(this,
                            new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.MANAGE_DOCUMENTS}, 0);
                }
                return;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }

        AdView mAdView = (AdView)findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("CFD62DC52635837D93B1BB6949AA1A6A").build();
        mAdView.loadAd(adRequest);


        shoppingButton = (ImageButton)findViewById(R.id.shoppingButton);
        shoppingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ShoppingMallListView();
            }
        });


        deliveryButton = (ImageButton) findViewById(R.id.deliveryButton);
        deliveryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DeliverySearchView();
            }
        });

        menuButton = (ImageButton) findViewById(R.id.menuButton);
        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popup = new PopupMenu(MainActivity.this, menuButton);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.popup_main, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {

                            case R.id.popup_request :

                                Intent intent = new Intent(Intent.ACTION_SENDTO);
                                intent.setData(Uri.parse("mailto:")); // only email apps should handle this
                                intent.putExtra(Intent.EXTRA_EMAIL,  new String[] { "phj050501@gmail.com" }) ;
                                intent.putExtra(Intent.EXTRA_SUBJECT, "장바구니 모아에게 문의합니다.");
                                if (intent.resolveActivity(getPackageManager()) != null) {
                                    startActivity(intent);
                                }

                                break;
                            case R.id.popup_review :

                                Intent marketLaunch = new Intent(Intent.ACTION_VIEW);
                                marketLaunch.setData(Uri.parse("market://details?id=com.chris.cartmoa"));
                                startActivity(marketLaunch);

                                break;

                            case R.id.popup_remove :

                                RemoveAllCart();

                                break;
                        }

                        return true;
                    }
                });

                popup.show();//showing popup menu

            }
        });


        recycler_view = (RecyclerView)findViewById(R.id.recycler_view);
        adapter = new CartAdapter(this);

        recycler_view.setAdapter(adapter);
        recycler_view.setLayoutManager(new LinearLayoutManager(this));


        mSpinner = (Spinner) findViewById(R.id.spinner_rss);

        String[] items = getResources().getStringArray(R.array.spinner_rss_items);
        List<String> spinnerItems = new ArrayList<String>();

        for(int i = 0; i < items.length; i++)
        {
            spinnerItems.add(items[i]);
        }

        final SpinnerAdapter adapter = new SpinnerAdapter(this, spinnerItems);
        mSpinner.setAdapter(adapter);

        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?>  parent, View view, int position, long id) {
              /*  Toast.makeText(getActivity(),
                        adspin.getItem(position) + "을 선택 했습니다.", 1).show();*/

                if(position == 0)
                {
                    sorting = "desc";
                    Sort(sorting);


                }
                else
                {
                    sorting = "asc";
                    Sort(sorting);
                }

            }
            public void onNothingSelected(AdapterView<?>  parent) {
            }
        });


   /*     if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
        {
            mSpinner.setDropDownVerticalOffset(-116);
        }*/


        mSwipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeLayout);
        mSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // 이 부분에 리플래시 시키고 싶으신 것을 넣어 주시면 됩니다.

                Sort(sorting);
            }
        });
        mSwipeRefresh.setDistanceToTriggerSync(500);

        final ImageButton search  = (ImageButton)findViewById(R.id.searchButton);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //ShowSearchMemory();
            }
        });

        final ImageButton fab = (ImageButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AddCartView();
            }
        });

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED){

            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE,  android.Manifest.permission.MANAGE_DOCUMENTS}, 0);
        }
        else if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED )
        {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE,  Manifest.permission.MANAGE_DOCUMENTS}, 0);
        }
        else {
            //canDo = true;
        }
    }

    long backPressedTime;
    long FINSH_INTERVAL_TIME = 2000;

    @Override
    public void onBackPressed() {
        long tempTime = System.currentTimeMillis();
        long intervalTime = tempTime - backPressedTime;

        if ( 0 <= intervalTime && FINSH_INTERVAL_TIME >= intervalTime ) {
            super.onBackPressed();
        } else {
            backPressedTime = tempTime;
            Toast.makeText(getApplicationContext(),"'뒤로'버튼을 한번 더 누르시면 종료됩니다.", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Sort(sorting);
    }

    private  void Sort(String value)
    {
        adapter.refreshData(value);
    }

    private void RemoveAllCart()
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setPositiveButton("비우기", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                adapter.Clear();
            }
        });

        alert.setNegativeButton("취소", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });

        alert.setTitle("장바구니 비우기");
        alert.setMessage("장바구니의 모든 아이템을 삭제합니다. 장바구니를 비우시겠습니까?");
        alert.show();
    }

    private  void ShowAlret(String title, String message)
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setPositiveButton("확인", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alert.setTitle(title);
        alert.setMessage(message);
        alert.show();
    }

    private void AddCartView()
    {
        final Intent intent = new Intent(this, AddCartActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        this.startActivityForResult(intent, 500);
    }

    private void ShoppingMallListView()
    {
        final Intent intent = new Intent(this, ShoppingMallListActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        this.startActivityForResult(intent, 500);
    }

    private void DeliverySearchView()
    {
        final Intent intent = new Intent(this, DeliverySearchActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        this.startActivityForResult(intent, 500);
    }

/*    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        finish();

        final Intent intent = new Intent(this, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        this.startActivity(intent);
    }*/

    @Override
    protected void onResume() {
        super.onResume();
        CrashManager.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        UpdateManager.unregister();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        UpdateManager.unregister();
    }
}
